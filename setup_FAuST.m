%% Description: setup_FAuST.m 
% Run script to set the useful paths in order to use the FAuST toolbox.
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%


ROOT_DIR=[fileparts(mfilename('fullpath')),filesep];

relpathlist={'',...
                  'proxs',...
                  'utils',...
                  'demos_and_applications',...
                  'toolboxes',...
                 };
             
fprintf('Welcome to FAuST_toolbox. FAuST root directory is %s\n',ROOT_DIR);
for k=1:numel(relpathlist)

    
    tmp_pathname=[ROOT_DIR,relpathlist{k}];
  fprintf('adding path %s\n',tmp_pathname);
  addpath(genpath(tmp_pathname));
end
cd(ROOT_DIR)
clear relpathlist tmp_pathname ROOT_DIR k

% Installation of a modified version of the SMALLbox
cd(['toolboxes',filesep,'smallbox_2.0'])
SMALLboxSetup
SMALLboxInit
cd('..');

% Installation of the CSC box

if ~exist(['CSCbox',filesep,'main_CSC.m'],'file')
    fprintf('\n Installation of the CSC box \n')
    websave('CSC_zip','http://gforge.inria.fr/frs/download.php/file/35882/CSCbox.zip');
    unzip('CSC_zip.zip')
    addpath(genpath('CSCbox'))
else
    fprintf('\n The CSC box is already installed \n')
end

% Installation of the GSP box

if ~exist(['gspbox',filesep,'gsp_install.m'],'file')
    fprintf('\n Installation of the GSP box \n')
    websave('GSP_zip','https://github.com/epfl-lts2/gspbox/releases/download/0.7.0/gspbox-0.7.0.zip');
    unzip('GSP_zip.zip')
    addpath(genpath('gspbox'))
    cd('gspbox');
    gsp_start
    gsp_make
    gsp_install
else
    fprintf('\n The GSP box is already installed \n')
end
cd(['..',filesep,'..']); 

fprintf('\n FAuST is successfully installed. \n')
