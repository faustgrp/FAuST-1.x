%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%	Source localization experiment	%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
General purpose:

In this directory, you will find code necessary to reproduce the results of 
Sec.V. of [1]. The folder "precomputed_results_MEG" contains also some 
pre-computed factorizations of the MEG gain matrix as well as the original
figures found in [1].

The MEG data comes from <http://martinos.org/mne/>. For more information, 
refers to [2].

For more information on the FAuST Project, please visit the website of the 
project: <http://faust.gforge.inria.fr>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Available tools:
-	function MEG_fact('demoType') [Optional]:
	Run the function MEG_fact.m" that will factorize the 
	MEG gain matrix found in "X_meg.mat" [2], and store the 
	factorization results in "./output/results_MEG_user.mat".

    The input parameter can only be 'example' (default value) or 'full'.   
    -   The 'example' input parameter propose *one* example of MEG 
        factorization, using a single set of parameters. 
        Computations should take around 8 minutes.
    -   The 'full' input parameter propose to reproduce the complete 
        results described in [1].
        WARNING: computation time for the full reproduction of results 
        from [1] can be long (several days) since this tests many 
        factorization settings. 
	
-	Fig_MEG.m: 
	Run this script to recreate Fig. 8. from [1] corresponding to
	the results of the factorization of the MEG matrix.	

	If you decided *not* to re-run the complete experiment with MEG_fact.m, 
	then the figure is obtained using the MEG data from the file
	"./precomputed_results_MEG/results_MEG.mat" 	

	Otherwise it is obtained from the file "./output/results_MEG_user.mat" 	
    
-	BSL.m [Optional]:
	Run this script that will use the factorized MEG gain matrices obtained 
	in the previous phase as well as the non-factorized one	to perform 
	source localization using the OMP algorithm, and store the results in 
	"./output/results_BSL_user.mat". 
	DURATION: Computations should take around 10 minutes. 

    The MEG gain matrices used are 
		- those in "./output/M_user.mat" if available
		- or the precomputed ones in "./precomputed_results_MEG/M_X.mat"

- 	Fig_BSL.m: 
	Run this script to recreate the Fig. 9. from [1] corresponding to 
	the localization performance.

	If you decided *not* to re-run the complete experiment with MEG_fact.m
	and/or BSL.m, then the figure is obtained using the file
	"./precomputed_results_MEG/results_BSL.mat"
	
	Otherwise it is obtained from the file "./output/results_BSL_user.mat". 	
	
- 	Fig_SVD.m:
	Run this script to recreate the Fig.2. from [1].

	If you decided *not* to re-run the complete experiment with MEG_fact.m
	and/or BSL.m, then the figure is obtained using the file
	"./precomputed_results_MEG/results_BSL.mat"
	
	Otherwise it is obtained from the file "./output/results_BSL_user.mat".

-   run_demo_precomputed.m is a script used to display all figures of the 
    article [1] concerning source localization, from precomputed data. 

-   run_demo_recompute_MEG.m is a script used to run full source 
    localisation demo, and display corresponding figures of the article [1]. 

-   run_demo_recomputeBSL.m performs brain source localization using 
    several gain matrices [2], including FAuSTs, and several solvers. 
    It reproduces the source localization experiment of [1], and display 
    corresponding figure.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
License:

Copyright (2016):	Luc Le Magoarou, Remi Gribonval
			INRIA Rennes, FRANCE
			http://www.inria.fr/

The FAuST Toolbox is distributed under the terms of the GNU Affero General 
Public License.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for 
more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Contacts:	
	Luc Le Magoarou: luc.le-magoarou@inria.fr
	Remi Gribonval : remi.gribonval@inria.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
References:

[1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
	approximations of matrices and applications", Journal of Selected 
	Topics in Signal Processing, 2016.
	<https://hal.archives-ouvertes.fr/hal-01167948v1>

[2]	A. Gramfort, M. Luessi, E. Larson, D. Engemann, D. Strohmeier, 
	C. Brodbeck, L. Parkkonen, M. Hamalainen, MNE software for processing
	MEG and EEG data <http://www.ncbi.nlm.nih.gov/pubmed/24161808>, 
	NeuroImage, Volume 86, 1 February 2014, Pages 446-460, ISSN 1053-8119, 
	[DOI] <http://dx.doi.org/10.1016/j.neuroimage.2013.10.027>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

