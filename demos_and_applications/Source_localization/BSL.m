%% Description BSL
%  Brain source localization
%
%  This script performs brain source localization using several gain
%  matrices [2], including FAuSTs, and several solvers. It reproduces the
%  source localization experiment of [1].
%  The results are stored in "./output/results_BSL_user.mat".
%  DURATION: Computations should take around 10 minutes. 
%	
%  The MEG gain matrices used are 
%		- those in "./output/M_user.mat" if available
%		- or the precomputed ones in "./precomputed_results_MEG/M_X.mat"
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%
% [2]   A. Gramfort, M. Luessi, E. Larson, D. Engemann, D. Strohmeier, 
%	C. Brodbeck, L. Parkkonen, M. Hamalainen, MNE software for processing
%	MEG and EEG data <http://www.ncbi.nlm.nih.gov/pubmed/24161808>, 
%	NeuroImage, Volume 86, 1 February 2014, Pages 446-460, ISSN 1053-8119, 
%	[DOI] <http://dx.doi.org/10.1016/j.neuroimage.2013.10.027>
%%

tic
runPath=which(mfilename);
pathname = fileparts(runPath);
precomputed_pathName=strcat(pathname,'/precomputed_results_MEG/');

%Loading the MEG matrix
load (strcat(pathname,'/X_meg.mat'));
points2 = points;
points = points(points_used_idx,:);
X = X_fixed;
M = X./repmat(sqrt(sum(X.^2,1)),size(X,1),1);

%Loading of the MEG matrix approximations
%load 'M_6.mat'
load (strcat(precomputed_pathName,'/M_6.mat'));
M_6 = lambda*dvp(facts);
M_6 = M_6'; M_6(:,sum(M_6.^2,1)==0)=1;
M_6_norm = M_6./repmat(sqrt(sum(M_6.^2,1)),size(M_6,1),1);

%load 'M_7.mat'
load (strcat(precomputed_pathName,'/M_7.mat'));
M_7 = lambda*dvp(facts);
M_7 = M_7'; M_7(:,sum(M_7.^2,1)==0)=1;
M_7_norm = M_7./repmat(sqrt(sum(M_7.^2,1)),size(M_7,1),1);

%load 'M_8.mat'
load (strcat(precomputed_pathName,'/M_8.mat'));
M_8 = lambda*dvp(facts);
M_8 = M_8'; M_8(:,sum(M_8.^2,1)==0)=1;
M_8_norm = M_8./repmat(sqrt(sum(M_8.^2,1)),size(M_8,1),1);

%load 'M_11.mat'
load (strcat(precomputed_pathName,'/M_11.mat'));
M_11 = lambda*dvp(facts);
M_11 = M_11'; M_11(:,sum(M_11.^2,1)==0)=1;
M_11_norm = M_11./repmat(sqrt(sum(M_11.^2,1)),size(M_11,1),1);

%load 'M_16.mat'
load (strcat(precomputed_pathName,'/M_16.mat'));
M_16 = lambda*dvp(facts);
M_16 = M_16'; M_16(:,sum(M_16.^2,1)==0)=1;
M_16_norm = M_16./repmat(sqrt(sum(M_16.^2,1)),size(M_16,1),1);

%load 'M_25.mat'
load (strcat(precomputed_pathName,'/M_25.mat'));
M_25 = lambda*dvp(facts);
M_25 = M_25'; M_25(:,sum(M_25.^2,1)==0)=1;
M_25_norm = M_25./repmat(sqrt(sum(M_25.^2,1)),size(M_25,1),1);

if exist(strcat(pathname,'/output/M_user.mat'),'file')
    load(strcat(pathname,'/output/M_user.mat'))
    M_user = lambda*dvp(facts);
    M_user = M_user'; M_user(:,sum(M_user.^2,1)==0)=1;
    M_user_norm = M_user./repmat(sqrt(sum(M_user.^2,1)),size(M_user,1),1);
end


Ntraining = 500; % Number of training vectors
Sparsity = 2; % Number of sources per training vector
dist_paliers = [0.01,0.05,0.08]; dist_paliers = [dist_paliers, 0.5];

resDist = zeros(7,3,numel(dist_paliers)-1,Sparsity,Ntraining); % (Matrice,method,dist_sources,src_nb,run);

for k=1:numel(dist_paliers)-1
    disp(['k=' num2str(k) '/' num2str(numel(dist_paliers)-1)])
    %Parameters settings
    Gamma = zeros(size(M,2),Ntraining);
    for ii=1:Ntraining
        %disp(num2str(ii));
        dist_sources = -1;
        while ~((dist_paliers(k)<dist_sources)&&(dist_sources<dist_paliers(k+1)))
            Gamma(:,ii) = sparse_coeffs(M, 1, Sparsity); %
            idx = find(Gamma(:,ii));
            dist_sources = norm(points(idx(1)) - points(idx(2)));
        end
        % dist_sources = norm(points(idx(1)) - points(idx(2)))
    end
    Data = M*Gamma;
    
    
    
    sol_omp = zeros(size(Gamma));
    sol_omp_hat = zeros(size(Gamma));
    sol_omp_hat2 = zeros(size(Gamma));
    err_omp = zeros(2,Ntraining);
    err_omp_hat = zeros(2,Ntraining);
    err_omp_hat2 = zeros(2,Ntraining);
    diff_omp = zeros(2,Ntraining);
    dist_omp = zeros(Sparsity,Ntraining);
    dist_omp_hat = zeros(Sparsity,Ntraining);
    dist_omp_hat2 = zeros(Sparsity,Ntraining);
    
    
    
    for i=1:Ntraining
        disp(['   i=' num2str(i) '/' num2str(Ntraining)])
        idx = find(Gamma(:,i));
        dist_sources = norm(points(idx(1)) - points(idx(2)));
        
        %OMP solving
        [sol_omp(:,i), err_mse_omp, iter_time_omp]=greed_omp_chol(Data(:,i),M,size(M,2),'stopTol',1*Sparsity);
        err_omp(1,i) = norm(M*Gamma(:,i)-M*sol_omp(:,i))/norm(M*Gamma(:,i));
        err_omp(2,i) = isequal(find(Gamma(:,i)),find(sol_omp(:,i)>1e-4));
        idx_omp = find(sol_omp(:,i));
        resDist(1,3,k,1,i) = min(norm(points(idx(1)) - points(idx_omp(1))),norm(points(idx(1)) - points(idx_omp(2))));
        resDist(1,3,k,2,i) = min(norm(points(idx(2)) - points(idx_omp(1))),norm(points(idx(2)) - points(idx_omp(2))));
        
        [sol_omp_hat(:,i), err_mse_omp_hat, iter_time_omp_hat]=greed_omp_chol(Data(:,i),M_6_norm,size(M_6_norm,2),'stopTol',1*Sparsity);
        err_omp_hat(1,i) = norm(M*Gamma(:,i)-M_6_norm*sol_omp_hat(:,i))/norm(M*Gamma(:,i));
        err_omp_hat(2,i) = isequal(find(Gamma(:,i)),find(sol_omp_hat(:,i)>1e-4));
        idx_omp_84 = find(sol_omp_hat(:,i));
        resDist(2,3,k,1,i) = min(norm(points(idx(1)) - points(idx_omp_84(1))),norm(points(idx(1)) - points(idx_omp_84(2))));
        resDist(2,3,k,2,i) = min(norm(points(idx(2)) - points(idx_omp_84(1))),norm(points(idx(2)) - points(idx_omp_84(2))));
        
        [sol_omp_hat(:,i), err_mse_omp_hat, iter_time_omp_hat]=greed_omp_chol(Data(:,i),M_7_norm,size(M_7_norm,2),'stopTol',1*Sparsity);
        err_omp_hat(1,i) = norm(M*Gamma(:,i)-M_7_norm*sol_omp_hat(:,i))/norm(M*Gamma(:,i));
        err_omp_hat(2,i) = isequal(find(Gamma(:,i)),find(sol_omp_hat(:,i)>1e-4));
        idx_omp_86 = find(sol_omp_hat(:,i));
        resDist(3,3,k,1,i) = min(norm(points(idx(1)) - points(idx_omp_86(1))),norm(points(idx(1)) - points(idx_omp_86(2))));
        resDist(3,3,k,2,i) = min(norm(points(idx(2)) - points(idx_omp_86(1))),norm(points(idx(2)) - points(idx_omp_86(2))));
        
        [sol_omp_hat(:,i), err_mse_omp_hat, iter_time_omp_hat]=greed_omp_chol(Data(:,i),M_8_norm,size(M_8_norm,2),'stopTol',1*Sparsity);
        err_omp_hat(1,i) = norm(M*Gamma(:,i)-M_8_norm*sol_omp_hat(:,i))/norm(M*Gamma(:,i));
        err_omp_hat(2,i) = isequal(find(Gamma(:,i)),find(sol_omp_hat(:,i)>1e-4));
        idx_omp_88 = find(sol_omp_hat(:,i));
        resDist(4,3,k,1,i) = min(norm(points(idx(1)) - points(idx_omp_88(1))),norm(points(idx(1)) - points(idx_omp_88(2))));
        resDist(4,3,k,2,i) = min(norm(points(idx(2)) - points(idx_omp_88(1))),norm(points(idx(2)) - points(idx_omp_88(2))));
        
        [sol_omp_hat(:,i), err_mse_omp_hat, iter_time_omp_hat]=greed_omp_chol(Data(:,i),M_11_norm,size(M_11_norm,2),'stopTol',1*Sparsity);
        err_omp_hat(1,i) = norm(M*Gamma(:,i)-M_11_norm*sol_omp_hat(:,i))/norm(M*Gamma(:,i));
        err_omp_hat(2,i) = isequal(find(Gamma(:,i)),find(sol_omp_hat(:,i)>1e-4));
        idx_omp_92 = find(sol_omp_hat(:,i));
        resDist(5,3,k,1,i) = min(norm(points(idx(1)) - points(idx_omp_92(1))),norm(points(idx(1)) - points(idx_omp_92(2))));
        resDist(5,3,k,2,i) = min(norm(points(idx(2)) - points(idx_omp_92(1))),norm(points(idx(2)) - points(idx_omp_92(2))));
        
        [sol_omp_hat(:,i), err_mse_omp_hat, iter_time_omp_hat]=greed_omp_chol(Data(:,i),M_16_norm,size(M_16_norm,2),'stopTol',1*Sparsity);
        err_omp_hat(1,i) = norm(M*Gamma(:,i)-M_16_norm*sol_omp_hat(:,i))/norm(M*Gamma(:,i));
        err_omp_hat(2,i) = isequal(find(Gamma(:,i)),find(sol_omp_hat(:,i)>1e-4));
        idx_omp_94 = find(sol_omp_hat(:,i));
        resDist(6,3,k,1,i) = min(norm(points(idx(1)) - points(idx_omp_94(1))),norm(points(idx(1)) - points(idx_omp_94(2))));
        resDist(6,3,k,2,i) = min(norm(points(idx(2)) - points(idx_omp_94(1))),norm(points(idx(2)) - points(idx_omp_94(2))));
        
        [sol_omp_hat(:,i), err_mse_omp_hat, iter_time_omp_hat]=greed_omp_chol(Data(:,i),M_25_norm,size(M_25_norm,2),'stopTol',1*Sparsity);
        err_omp_hat(1,i) = norm(M*Gamma(:,i)-M_25_norm*sol_omp_hat(:,i))/norm(M*Gamma(:,i));
        err_omp_hat(2,i) = isequal(find(Gamma(:,i)),find(sol_omp_hat(:,i)>1e-4));
        idx_omp_96 = find(sol_omp_hat(:,i));
        resDist(7,3,k,1,i) = min(norm(points(idx(1)) - points(idx_omp_96(1))),norm(points(idx(1)) - points(idx_omp_96(2))));
        resDist(7,3,k,2,i) = min(norm(points(idx(2)) - points(idx_omp_96(1))),norm(points(idx(2)) - points(idx_omp_96(2))));

        if exist('M_user_norm','var')
            [sol_omp_hat(:,i), err_mse_omp_hat, iter_time_omp_hat]=greed_omp_chol(Data(:,i),M_user_norm,size(M_user_norm,2),'stopTol',1*Sparsity);
            err_omp_hat(1,i) = norm(M*Gamma(:,i)-M_user_norm*sol_omp_hat(:,i))/norm(M*Gamma(:,i));
            err_omp_hat(2,i) = isequal(find(Gamma(:,i)),find(sol_omp_hat(:,i)>1e-4));
            idx_omp_user = find(sol_omp_hat(:,i));
            resDist(8,3,k,1,i) = min(norm(points(idx(1)) - points(idx_omp_user(1))),norm(points(idx(1)) - points(idx_omp_user(2))));
            resDist(8,3,k,2,i) = min(norm(points(idx(2)) - points(idx_omp_user(1))),norm(points(idx(2)) - points(idx_omp_user(2))));
        end
        %save('results_current','resDist');
        
    end
end
toc
heure = clock ;

%save(['results_BSL_user'],'resDist');

runPath=which(mfilename);
pathname = fileparts(runPath);
if ( exist(strcat(pathname,'/output'),'dir') == 0 )
    mkdir ( strcat(pathname,'/output/'));
end   
matfile = fullfile(pathname, 'output/results_BSL_user');
save(matfile,'resDist');

