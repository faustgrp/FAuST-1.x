%% Description Fig_BSL 
%  Brain source localization figures
%  This script builds the BSL figure (Fig. 9.) used in [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%
% [2]   A. Gramfort, M. Luessi, E. Larson, D. Engemann, D. Strohmeier, 
%	C. Brodbeck, L. Parkkonen, M. Hamalainen, MNE software for processing
%	MEG and EEG data <http://www.ncbi.nlm.nih.gov/pubmed/24161808>, 
%	NeuroImage, Volume 86, 1 February 2014, Pages 446-460, ISSN 1053-8119, 
%	[DOI] <http://dx.doi.org/10.1016/j.neuroimage.2013.10.027>
%%

if (not(exist('precomputed_data')))
    precomputed_data='FALSE';
end

runPath=which(mfilename);
pathname = fileparts(runPath);
if ( exist(strcat(pathname,'/output/results_BSL_user.mat'),'file') && not(strcmp(precomputed_data,'TRUE' )))
    %load 'results_BSL_user'
    load (strcat(strcat(pathname,'/output/'),'results_BSL_user')) ;
    disp(strcat('Using your results from directory :',strcat(pathname,'/output/results_BSL_user.mat')));
elseif exist(strcat(pathname,'/precomputed_results_MEG/results_BSL.mat'),'file')
    %load 'results_BSL'
    load (strcat(strcat(pathname,'/precomputed_results_MEG/'),'results_BSL')) ;
    disp(strcat('Using existing results (from reference [1]) from directory :',strcat(pathname,'/precomputed_results_MEG/results_BSL.mat')));
else
    error(['ERROR : A problem occurred when loading the results of BSL.']);
end

matRes = zeros(3,5,3);%zeros(10,3,3);
for i=1:3
    for j =1:5
        for k =1:3
            matRes(i,j,k) = 1 - numel(find(resDist(j,k,i,:,:)>0.003))/1000;
            %matRes(i,j,k) = mean(mean(resDist(j,k,i,:,:),4),5);
            %matRes(i,j,k) = mean(resDist(j,k,i,:,:),4);
        end
    end
end



%  d1 = cat(5,resDist(:,1,1,1,:),resDist(:,1,1,2,:));
%  d2 = cat(5,resDist(:,1,2,1,:),resDist(:,1,2,2,:));
%  d3 = cat(5,resDist(:,1,3,1,:),resDist(:,1,3,2,:));
%  test2 = 100*[squeeze(d1);zeros(1,1000);squeeze(d2);zeros(1,1000);squeeze(d3)];
%boxPlot(test2')
%  figure('color',[1 1 1]);
%  T = bplot(test2','linewidth',1.5);
%  legend(T)
%  title('l_1ls')
%  ylabel('Distance between true and estimated sources (cm)')
%  box on
%  ax = gca;
%  set(ax,'XTick',[1:5,7:11,13:17]);
% set(ax,'XtickLabel',{'X','X_{84}','X_{88}','X_{94}','$X_{96}'});%{'1<d<5','5<d<8','d>8'}
%
% set(ax,'xticklabel', [])
% axi = axis;
% HorizontalOffset = 10;
% verticalOffset = 0.26;
% verticalOffset2 = 0.8;
% yTicks = get(ax,'ytick');
% xTicks = get(ax, 'xtick');
% minY = min(yTicks);
%
% text(xTicks(1), minY - verticalOffset, '$\mathbf{X}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(2), minY - verticalOffset, '$\hat{\mathbf{X}}_{6}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(3), minY - verticalOffset, '$\hat{\mathbf{X}}_{8}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(4), minY - verticalOffset, '$\hat{\mathbf{X}}_{16}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(5), minY - verticalOffset, '$\hat{\mathbf{X}}_{25}$','HorizontalAlignment','center','interpreter', 'latex');
%
% text(xTicks(6), minY - verticalOffset, '$\mathbf{X}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(7), minY - verticalOffset, '$\hat{\mathbf{X}}_{6}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(8), minY - verticalOffset, '$\hat{\mathbf{X}}_{8}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(9), minY - verticalOffset, '$\hat{\mathbf{X}}_{16}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(10), minY - verticalOffset, '$\hat{\mathbf{X}}_{25}$','HorizontalAlignment','center','interpreter', 'latex');
%
% text(xTicks(11), minY - verticalOffset, '$\mathbf{X}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(12), minY - verticalOffset, '$\hat{\mathbf{X}}_{6}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(13), minY - verticalOffset, '$\hat{\mathbf{X}}_{8}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(14), minY - verticalOffset, '$\hat{\mathbf{X}}_{16}$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(15), minY - verticalOffset, '$\hat{\mathbf{X}}_{25}$','HorizontalAlignment','center','interpreter', 'latex');
%
% text(xTicks(3), minY - verticalOffset2, '$1<d<5$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(8), minY - verticalOffset2, '$5<d<8$','HorizontalAlignment','center','interpreter', 'latex');
% text(xTicks(13), minY - verticalOffset2, '$d>8$','HorizontalAlignment','center','interpreter', 'latex');


d1 = cat(5,resDist(:,3,1,1,:),resDist(:,3,1,2,:));
d2 = cat(5,resDist(:,3,2,1,:),resDist(:,3,2,2,:));
d3 = cat(5,resDist(:,3,3,1,:),resDist(:,3,3,2,:));
test2 = 100*[squeeze(d1);zeros(1,1000);squeeze(d2);zeros(1,1000);squeeze(d3)];
%boxPlot(test2')
figure('color',[1 1 1]);
T = bplot(test2','linewidth',1.5);
legend(T)
% title('OMP')
ylabel('Distance between true and estimated sources (cm)')
box on
ax = gca;
set(ax,'XTick',[1:7,9:15,17:23]);
set(ax,'xticklabel', [])
axi = axis;
axis([0 24 -0.3 4.7])
HorizontalOffset = 10;
verticalOffset = 0.43;
verticalOffset2 = 0.7;
yTicks = get(ax,'ytick');
xTicks = get(ax, 'xtick');
minY = min(yTicks);

text(xTicks(1), minY - verticalOffset, '$\mathbf{M}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(2), minY - verticalOffset, '$\widehat{\mathbf{M}}_{6}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(3), minY - verticalOffset, '$\widehat{\mathbf{M}}_{7}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(4), minY - verticalOffset, '$\widehat{\mathbf{M}}_{8}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(5), minY - verticalOffset, '$\widehat{\mathbf{M}}_{11}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(6), minY - verticalOffset, '$\widehat{\mathbf{M}}_{16}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(7), minY - verticalOffset, '$\widehat{\mathbf{M}}_{25}$','HorizontalAlignment','center','interpreter', 'latex');

text(xTicks(8), minY - verticalOffset, '$\mathbf{M}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(9), minY - verticalOffset, '$\widehat{\mathbf{M}}_{6}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(10), minY - verticalOffset, '$\widehat{\mathbf{M}}_{7}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(11), minY - verticalOffset, '$\widehat{\mathbf{M}}_{8}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(12), minY - verticalOffset, '$\widehat{\mathbf{M}}_{11}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(13), minY - verticalOffset, '$\widehat{\mathbf{M}}_{16}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(14), minY - verticalOffset, '$\widehat{\mathbf{M}}_{25}$','HorizontalAlignment','center','interpreter', 'latex');

text(xTicks(15), minY - verticalOffset, '$\mathbf{M}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(16), minY - verticalOffset, '$\widehat{\mathbf{M}}_{6}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(17), minY - verticalOffset, '$\widehat{\mathbf{M}}_{7}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(18), minY - verticalOffset, '$\widehat{\mathbf{M}}_{8}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(19), minY - verticalOffset, '$\widehat{\mathbf{M}}_{11}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(20), minY - verticalOffset, '$\widehat{\mathbf{M}}_{16}$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(21), minY - verticalOffset, '$\widehat{\mathbf{M}}_{25}$','HorizontalAlignment','center','interpreter', 'latex');

text(xTicks(4), minY - verticalOffset2, '$1<d<5$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(11), minY - verticalOffset2, '$5<d<8$','HorizontalAlignment','center','interpreter', 'latex');
text(xTicks(18), minY - verticalOffset2, '$d>8$','HorizontalAlignment','center','interpreter', 'latex');




% figure
% subplot_tight(1,3,1,0.07)
% imagesc(matRes(:,:,1));%imagesc(matRes(:,[1 3 2],1));
% colormap gray
% caxis([min(min(min(matRes))) max(max(max(matRes)))])
%
% subplot_tight(1,3,2,0.07)
% imagesc(matRes(:,:,2));
% caxis([min(min(min(matRes))) max(max(max(matRes)))])
%
% subplot_tight(1,3,3,0.07)
% imagesc(matRes(:,:,3));
% caxis([min(min(min(matRes))) max(max(max(matRes)))])



% figure('color',[1 1 1])
% subplot_tight(2,1,1,0.08)
% bar([sum(matRes(1,:,1),1);sum(matRes(2,:,1),1);sum(matRes(3,:,1),1)]);%bar([sum(matRes(2:5,[1 3 2],1),1)/4;sum(matRes(6:8,[1 3 2],1),1)/3;sum(matRes(9:10,[1 3 2],1),1)/2]);
% %bplot([sum(matRes(1,:,1),1);sum(matRes(2,:,1),1);sum(matRes(3,:,1),1)])
% colormap summer
% axis([0.5,3.5,0.4,0.85])
% l=legend('$\mathbf{X}$','$\hat{\mathbf{X}}_{84}$','$\hat{\mathbf{X}}_{88}$','$\hat{\mathbf{X}}_{94}$','$\hat{\mathbf{X}}_{96}$');
% set(l,'interpreter','latex','location','northwest');
% set(l,'FontSize',14);
% title('l_lls','FontSize',12);
% xlabel('Distance between sources (cm)','FontSize',12);
% ylabel('Fraction of retrieved sources','FontSize',12);
% ax = gca;
% set(ax,'XtickLabel',{'1<d<5','5<d<8','d>8'});
% %set(ax.XtickLabel,'interpreter','latex');
% %ax.XTickLabel = {'1<d<5','5<d<8','8<d'};

% subplot_tight(2,1,2,0.08)
% bar([sum(matRes(1,:,3),1);sum(matRes(2,:,3),1);sum(matRes(3,:,3),1)]);
% colormap summer
% axis([0.5,3.5,0.4,0.85])
% %l=legend('$\mathbf{X}$','$\hat{\mathbf{X}}_1$','$\hat{\mathbf{X}}_2$');
% %set(l,'interpreter','latex','location','northwest');
% title('OMP','FontSize',12);
% xlabel('Distance between sources (cm)','FontSize',12);
% ylabel('Fraction of retrieved sources','FontSize',12);
% ax = gca;
% set(ax,'XtickLabel',{'1<d<5','5<d<8','d>8'});
% %set(ax.XtickLabel,'interpreter','latex');
% %ax.XTickLabel = {'1<d<5','5<d<8','8<d'};
