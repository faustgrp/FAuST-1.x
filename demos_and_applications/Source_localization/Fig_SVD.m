%% Description Fig_SVD 
%  SVD figure
%  This script builds the SVD figure (Fig. 2.) used in [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%
% [2]   A. Gramfort, M. Luessi, E. Larson, D. Engemann, D. Strohmeier, 
%	C. Brodbeck, L. Parkkonen, M. Hamalainen, MNE software for processing
%	MEG and EEG data <http://www.ncbi.nlm.nih.gov/pubmed/24161808>, 
%	NeuroImage, Volume 86, 1 February 2014, Pages 446-460, ISSN 1053-8119, 
%	[DOI] <http://dx.doi.org/10.1016/j.neuroimage.2013.10.027>
%%


%Loading the MEG matrix
load 'X_meg.mat'

[U_meg,S_meg,V_meg] = svd(X_fixed,'econ');
s = diag(S_meg);
err = flipud(s/s(1));

RC = (204+8193)*(204-(1:204))/(204*8193);

fig=figure('Color',[1 1 1]);hold on;

% Create axes
axis off
axes1 = axes(...%'ZTick',[-1 0 1],...
    'YTick',[0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1],...
    'XTickLabel',{'10','5','3.33','2.5','2','1.67'},...
    'XTick',[0.1 0.2 0.3 0.4 0.5 0.6],...
    'FontSize',20);
    %'Position',[0.07 0.07 0.86 0.86]
    
box(axes1,'on');
hold(axes1,'on');


%subplot_tight(1,1,1,0.07)
plot(RC,err)
%title('Complexity/accuracy tradeoff of the SVD')
%xlabel('Relative Complexity (RC)')
%ylabel('Relative error')

if (not(exist('precomputed_data')))
    precomputed_data='FALSE';
end

%load 'results_MEG'
runPath=which(mfilename);
pathname = fileparts(runPath);
if ( exist(strcat(pathname,'/output/results_MEG_user.mat'),'file') && not(strcmp(precomputed_data,'TRUE' )))
    %load 'results_MEG_user'
    load (strcat(strcat(pathname,'/output/'),'results_MEG_user')) ;
    disp(strcat('Using your results from directory :',strcat(pathname,'/output/results_MEG_user.mat')));
elseif exist(strcat(pathname,'/precomputed_results_MEG/results_MEG.mat'),'file')
    %load 'results_MEG'
    load (strcat(strcat(pathname,'/precomputed_results_MEG/'),'results_MEG')) ;
    disp(strcat('Using existing results (from reference [1]) from directory :',strcat(pathname,'/precomputed_results_MEG/results_MEG.mat')));
else
    error(['ERROR : A problem occurred when loading the results of MEG data.']);
end

marktab = ['.','o','*','s','p','d','^','x','>'];


hold on

marksize=6;
col = [2/(size(resultTab,2)-1),1-2/(size(resultTab,2)-1),0];
% X96 = plot(resultTab(1,3,1,1,9,2),resultTab(1,3,1,1,9,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
% X94 = plot(resultTab(2,3,1,1,7,2),resultTab(2,3,1,1,7,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
% X92 = plot(resultTab(3,3,1,1,5,2),resultTab(3,3,1,1,5,1),'marker',marktab(5),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
% X88 = plot(resultTab(3,1,1,1,4,2),resultTab(3,1,1,1,4,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
% X86 = plot(resultTab(5,1,1,1,4,2),resultTab(5,1,1,1,4,1),'marker',marktab(4),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
% X84 = plot(resultTab(4,1,1,1,5,2),resultTab(4,1,1,1,5,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);

X96 = plot(resultTab(1,3,1,1,9,2),resultTab(1,3,1,1,9,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
X94 = plot(resultTab(2,3,1,1,8,2),resultTab(2,3,1,1,8,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
X92 = plot(resultTab(3,3,1,1,5,2),resultTab(3,3,1,1,5,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
%col = [0/(size(resultTab,2)-1),1-0/(size(resultTab,2)-1),0];
X88 = plot(resultTab(4,1,1,1,4,2),resultTab(4,1,1,1,4,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
X86 = plot(resultTab(5,1,1,1,4,2),resultTab(5,1,1,1,4,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
X84 = plot(resultTab(6,1,1,1,5,2),resultTab(6,1,1,1,5,1),'marker',marktab(2),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);


axis([0,0.6,0,1])
% Create ylabel
ylabel('Relative error','FontSize',24,'FontName','Arial');

% Create xlabel
xlabel('Relative Complexity Gain (RCG)','FontSize',24,'FontName','Arial');

% Create textarrow
annotation(fig,'textarrow',[0.271634382566586 0.253200234132438],...
    [0.20010375709917 0.163406509392748],'Color',[1 0 0],'String',{'FAuST'},...
    'HeadWidth',6,...
    'HeadStyle','plain',...
    'HeadLength',6,...
    'FontSize',30,...
    'FontName','Arial');

% Create textarrow
annotation(fig,'textarrow',[0.296774042513344 0.262243655773013],...
    [0.392930374042361 0.350343623253717],...
    'Color',[0 0.447058823529412 0.741176470588235],...
    'String',{'SVD'},...
    'HeadWidth',6,...
    'HeadStyle','plain',...
    'HeadLength',6,...
    'FontSize',30,...
    'FontName','Arial');
 
