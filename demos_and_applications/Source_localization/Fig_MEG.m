%% Description Fig_MEG 
%  MEG factorization figure
%  This script builds the MEG factorization figure (Fig. 8.) used in [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%
% [2]   A. Gramfort, M. Luessi, E. Larson, D. Engemann, D. Strohmeier, 
%	C. Brodbeck, L. Parkkonen, M. Hamalainen, MNE software for processing
%	MEG and EEG data <http://www.ncbi.nlm.nih.gov/pubmed/24161808>, 
%	NeuroImage, Volume 86, 1 February 2014, Pages 446-460, ISSN 1053-8119, 
%	[DOI] <http://dx.doi.org/10.1016/j.neuroimage.2013.10.027>
%%

if (not(exist('precomputed_data')))
    precomputed_data='FALSE';
end

runPath=which(mfilename);
pathname = fileparts(runPath);
if (exist(strcat(pathname,'/output/results_MEG_user.mat'),'file') && not (strcmp(precomputed_data,'TRUE' )))
    %load 'results_MEG_user'
    % loading the matrice result_allglob :
    load ('results_MEG','result_allglob');
    load (strcat(strcat(pathname,'/output/'),'results_MEG_user')) ;
    disp(strcat('Using your results from directory :',strcat(pathname,'/output/results_MEG_user.mat')));
elseif exist(strcat(pathname,'/precomputed_results_MEG/results_MEG.mat'),'file')
    %load 'results_MEG'
    load (strcat(strcat(pathname,'/precomputed_results_MEG/'),'results_MEG')) ;
    disp(strcat('Using existing results (from reference [1]) from directory :',strcat(pathname,'/precomputed_results_MEG/results_MEG.mat')));
else
    error(['ERROR : A problem occurred when loading the results of MEG data.']);
end


marktab = ['.','o','*','s','p','x','^','d','>'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fig=figure('Color',[1 1 1]);
hold on


subplot_tight(1,1,1,0.07)
hold on
%plotTab = zeros([size(resultTab,1),size(resultTab,2),size(resultTab,3),size(resultTab,4)]);
plotTab = zeros([size(resultTab,1),size(resultTab,2),size(resultTab,3),size(resultTab,4),size(resultTab,5)]);
for  i=1:size(resultTab,1)
    for  j=1:size(resultTab,2)
        for  k=1:size(resultTab,3)
            for l=1
                for m=1:size(resultTab,5)
                    if m==1
                        col = [0,0,0];
                        marksize = 20;
                    else
                        col = [(j-1)/(size(resultTab,2)-1),1-(j-1)/(size(resultTab,2)-1),0];
                        marksize = 6;
                    end
                    plotTab(i,j,k,l,m) = plot(resultTab(i,j,k,l,m,2),resultTab(i,j,k,l,m,1),'marker',marktab(m),'markersize',marksize,'linestyle','none','color',col);
                    %plot(resultTab(i,j,k,l,2),resultTab(i,j,k,l,1),'marker','.','markersize',8)
                end
            end
        end
    end
end
col = [2/(size(resultTab,2)-1),1-2/(size(resultTab,2)-1),0];
X96 = plot(resultTab(1,3,1,1,9,2),resultTab(1,3,1,1,9,1),'marker',marktab(9),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
X94 = plot(resultTab(2,3,1,1,8,2),resultTab(2,3,1,1,8,1),'marker',marktab(8),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
X92 = plot(resultTab(3,3,1,1,5,2),resultTab(3,3,1,1,5,1),'marker',marktab(5),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
col = [0/(size(resultTab,2)-1),1-0/(size(resultTab,2)-1),0];
X88 = plot(resultTab(4,1,1,1,4,2),resultTab(4,1,1,1,4,1),'marker',marktab(4),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
X86 = plot(resultTab(5,1,1,1,4,2),resultTab(5,1,1,1,4,1),'marker',marktab(4),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
X84 = plot(resultTab(6,1,1,1,5,2),resultTab(6,1,1,1,5,1),'marker',marktab(5),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);

col = [0/(size(resultTab,2)-1),1-0/(size(resultTab,2)-1),0];
tst = plot(squeeze(resultTab(6,1,1,1,:,2)),squeeze(resultTab(6,1,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(5,1,1,1,:,2)),squeeze(resultTab(5,1,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(4,1,1,1,:,2)),squeeze(resultTab(4,1,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(3,1,1,1,:,2)),squeeze(resultTab(3,1,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(2,1,1,1,:,2)),squeeze(resultTab(2,1,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(1,1,1,1,:,2)),squeeze(resultTab(1,1,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);

col = [1/(size(resultTab,2)-1),1-1/(size(resultTab,2)-1),0];
tst = plot(squeeze(resultTab(6,2,1,1,:,2)),squeeze(resultTab(6,2,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(5,2,1,1,:,2)),squeeze(resultTab(5,2,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(4,2,1,1,:,2)),squeeze(resultTab(4,2,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(3,2,1,1,:,2)),squeeze(resultTab(3,2,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(2,2,1,1,:,2)),squeeze(resultTab(2,2,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(1,2,1,1,:,2)),squeeze(resultTab(1,2,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);

col = [2/(size(resultTab,2)-1),1-2/(size(resultTab,2)-1),0];
tst = plot(squeeze(resultTab(6,3,1,1,:,2)),squeeze(resultTab(6,3,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(5,3,1,1,:,2)),squeeze(resultTab(5,3,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(4,3,1,1,:,2)),squeeze(resultTab(4,3,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(3,3,1,1,:,2)),squeeze(resultTab(3,3,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(2,3,1,1,:,2)),squeeze(resultTab(2,3,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
tst = plot(squeeze(resultTab(1,3,1,1,:,2)),squeeze(resultTab(1,3,1,1,:,1)),'marker','none','markersize',marksize,'linestyle','-','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);


j2 = plot([10 10], [10 11], 'g-','Color',[(1-1)/(size(resultTab,2)-1),1-(1-1)/(size(resultTab,2)-1),0],'LineWidth',3);
j4 = plot([10 10], [10 11], 'g-','Color',[(2-1)/(size(resultTab,2)-1),1-(2-1)/(size(resultTab,2)-1),0],'LineWidth',3);
j8 = plot([10 10], [10 11], 'g-','Color',[(3-1)/(size(resultTab,2)-1),1-(3-1)/(size(resultTab,2)-1),0],'LineWidth',3);


p1 = plot(result_allglob(1,3,1,1,9,2),result_allglob(1,3,1,1,9,1),'marker',marktab(9),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col);
p1bis = plot([result_allglob(1,3,1,1,9,2),resultTab(1,3,1,1,9,2)],[result_allglob(1,3,1,1,9,1),resultTab(1,3,1,1,9,1)],'marker','none','markersize',marksize,'linestyle',':','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
p2 = plot(result_allglob(2,3,1,1,8,2),result_allglob(2,3,1,1,8,1),'marker',marktab(8),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col);
p2bis = plot([result_allglob(2,3,1,1,8,2),resultTab(2,3,1,1,8,2)],[result_allglob(2,3,1,1,8,1),resultTab(2,3,1,1,8,1)],'marker','none','markersize',marksize,'linestyle',':','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
p3 = plot(result_allglob(3,3,1,1,5,2),result_allglob(3,3,1,1,5,1),'marker',marktab(5),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col);
p3bis = plot([result_allglob(3,3,1,1,5,2),resultTab(3,3,1,1,5,2)],[result_allglob(3,3,1,1,5,1),resultTab(3,3,1,1,5,1)],'marker','none','markersize',marksize,'linestyle',':','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
col = [0/(size(resultTab,2)-1),1-0/(size(resultTab,2)-1),0];
p4 = plot(result_allglob(4,1,1,1,4,2),result_allglob(4,1,1,1,4,1),'marker',marktab(4),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col);
p4bis = plot([result_allglob(4,1,1,1,4,2),resultTab(4,1,1,1,4,2)],[result_allglob(4,1,1,1,4,1),resultTab(4,1,1,1,4,1)],'marker','none','markersize',marksize,'linestyle',':','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
p5 = plot(result_allglob(5,1,1,1,4,2),result_allglob(5,1,1,1,4,1),'marker',marktab(4),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col);
p5bis = plot([result_allglob(5,1,1,1,4,2),resultTab(5,1,1,1,4,2)],[result_allglob(5,1,1,1,4,1),resultTab(5,1,1,1,4,1)],'marker','none','markersize',marksize,'linestyle',':','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);
p6 = plot(result_allglob(6,1,1,1,5,2),result_allglob(6,1,1,1,5,1),'marker',marktab(5),'markersize',marksize,'linestyle','none','color',col,'MarkerEdgeColor',col);
p6bis = plot([result_allglob(6,1,1,1,5,2),resultTab(6,1,1,1,5,2)],[result_allglob(6,1,1,1,5,1),resultTab(6,1,1,1,5,1)],'marker','none','markersize',marksize,'linestyle',':','color',col,'MarkerEdgeColor',col,'MarkerFaceColor',col);


annotation('textarrow',[0.15,0.15],[0.84,0.79],...
           'String','$k=5$','fontsize',14,'interpreter','latex');
annotation('textarrow',[0.29,0.29],[0.47,0.42],...
           'String','$k=10$','fontsize',14,'interpreter','latex');
annotation('textarrow',[0.43,0.43],[0.42,0.37],...
           'String','$k=15$','fontsize',14,'interpreter','latex');
annotation('textarrow',[0.58,0.58],[0.34,0.29],...
           'String','$k=20$','fontsize',14,'interpreter','latex');
annotation('textarrow',[0.71,0.71],[0.33,0.28],...
           'String','$k=25$','fontsize',14,'interpreter','latex');
annotation('textarrow',[0.85,0.85],[0.32,0.27],...
           'String','$k=30$','fontsize',14,'interpreter','latex');       
axis ([0.03,0.18,0,0.15])
%leg = legend([j2,j4,j8,X96,X94,X88,X84],'$s = 2$','$s = 4$','$s = 8$','$\hat{\mathbf{X}}_{25}$','$\hat{\mathbf{X}}_{17}$','$\hat{\mathbf{X}}_{8}$','$\hat{\mathbf{X}}_{6}$');
leg = legend([j2,j4,j8],'$s/m = 2$','$s/m = 4$','$s/m = 8$');
legendmarkeradjust(8)
%textobj = findobj(leg, 'type', 'text');
%set(textobj, 'Interpreter', 'latex', 'fontsize', 12);
set(leg,'interpreter','latex')
set(leg,'FontSize',16);
set(gca,'Box','on')
%title(['$ q = 1.2 $'],'interpreter','latex')
ylabel('Relative operator norm error','fontsize',14);
xlabel('Relative Complexity Gain (RCG)','fontsize',14);
xticks = get(gca,'xtick');
set(gca,'xticklabel',num2cell(round(100./xticks)/100));


% Create textarrow
annotation(fig,'textarrow',[0.102295472444726 0.113240746076567],...
    [0.598676242733951 0.643163670200102],...
    'String',{'$\widehat{\mathbf{M}}_{25}$'},...
    'Interpreter','latex',...
    'FontName','Helvetica');

% Create textarrow
annotation(fig,'textarrow',[0.538738738738738 0.560828290977541],...
    [0.140406049269286 0.171972780410485],...
    'String',{'$\widehat{\mathbf{M}}_{8}$'},...
    'Interpreter','latex',...
    'FontName','Helvetica');

% Create textarrow
annotation(fig,'textarrow',[0.402604166666667 0.421875],...
    [0.165091610414658 0.198649951783992],...
    'String',{'$\widehat{\mathbf{M}}_{11}$'},...
    'Interpreter','latex',...
    'FontName','Helvetica');

% Create textarrow
annotation(fig,'textarrow',[0.684375 0.7046875],...
    [0.133558341369335 0.162970106075217],...
    'String',{'$\widehat{\mathbf{M}}_{7}$'},...
    'Interpreter','latex',...
    'FontName','Helvetica');

% Create textarrow
annotation(fig,'textarrow',[0.805559203477899 0.826041666666667],...
    [0.1306655967554 0.160077145612343],'String',{'$\widehat{\mathbf{M}}_{6}$'},...
    'Interpreter','latex',...
    'FontName','Helvetica');

% Create textarrow
annotation(fig,'textarrow',[0.239108462819724 0.258333333333333],...
    [0.232846587047613 0.273866923818708],...
    'String',{'$\widehat{\mathbf{M}}_{16}$'},...
    'Interpreter','latex',...
    'FontName','Helvetica');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

