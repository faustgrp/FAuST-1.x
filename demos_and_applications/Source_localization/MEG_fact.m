function MEG_fact(demoType)
%% function MEG_fact('example')
%  Function used to run Factorization of a MEG matrix.
%
%  This function performs  a hierarchical factorization of a MEG gain
%  matrix [2] into several sparse factors. 
%  The factorization is done using the hierarchical factorization 
%  described in [1, Fig.5].
%  
%  The input parameter can only be 'example' (default value) or 'full'.   
%   - The default value 'example' input parameter propose *one* example of 
%     MEG factorization, using a single set of parameters. 
%     Computations should take around 8 minutes.
%   - The 'full' input parameter propose to reproduce the complete results
%     described in [1].
%     WARNING: computation time for the full reproduction of results from [1]
%     can be long (several days) since this tests many factorization settings. 
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%
% [2]   A. Gramfort, M. Luessi, E. Larson, D. Engemann, D. Strohmeier, 
%	C. Brodbeck, L. Parkkonen, M. Hamalainen, MNE software for processing
%	MEG and EEG data <http://www.ncbi.nlm.nih.gov/pubmed/24161808>, 
%	NeuroImage, Volume 86, 1 February 2014, Pages 446-460, ISSN 1053-8119, 
%	[DOI] <http://dx.doi.org/10.1016/j.neuroimage.2013.10.027>
%%

%clear
%close all
%clc

    tic
    %%%% Setting optional parameters values %%%%
    if (strcmp(demoType,'full'))
        disp('Running full demonstration : WARNING this demonstration required several days.');
        disp('Are you sure you want to run this full demonstration?');
        pause(1);

        N_FACTS = 10; % Desired number of factors
        N_FACTS_MAX = 10; % Maximal Desired number of factors
        P_REP_tab = [5,10,15,20,25,30]; % Sparsity of the rightmost factor's columns
        s_tab = [2,4,8];%[2,4,8,16,32]; Mean sparsity of the inner factor's columns
    elseif (strcmp(demoType,'example'))
        N_FACTS = 5; % Desired number of factors
        % NFACTS = 10; % Desired number of factors
        N_FACTS_MAX = 10; % Maximal Desired number of factors
        P_REP_tab = [20]; % Sparsity of the rightmost factor's columns
        s_tab = [2];%[2,4,8,16,32]; Mean sparsity of the inner factor's columns
        disp('Running an example of the demonstration with following parameters.');
        disp(strcat('Desired number of factors         : ', num2str(N_FACTS)));
        disp(strcat('Sparsity of the rightmost factor  : ', num2str(P_REP_tab)));
        disp(strcat('Mean sparsity of the inner factor : ', num2str(s_tab)));
        pause(1);
    else
        disp ('WARNING: The input value is not recognized. The default parameter "example" is used.') 
        N_FACTS = 5; % Desired number of factors
        % NFACTS = 10; % Desired number of factors
        N_FACTS_MAX = 10; % Maximal Desired number of factors
        P_REP_tab = [20]; % Sparsity of the rightmost factor's columns
        s_tab = [2];%[2,4,8,16,32]; Mean sparsity of the inner factor's columns
        disp('Running an example of the demonstration with following parameters.');
        disp(strcat('Desired number of factors         : ', num2str(N_FACTS)));
        disp(strcat('Sparsity of the rightmost factor  : ', num2str(P_REP_tab)));
        disp(strcat('Mean sparsity of the inner factor : ', num2str(s_tab)));
        pause(1);
    end
       
    runPath=which(mfilename);
    pathname = fileparts(runPath);
    if ( exist(strcat(pathname,'/output'),'dir') == 0 )
        mkdir ( strcat(pathname,'/output/'));
    end

    %% Generating the data
    load (strcat(pathname,'/X_meg.mat'));
    X = X_fixed;
    params.data = X';
    DIM = size(X,1); % Dimension of the signals
    N_SAMP = size(X,2); % Number of training signals
    
    P_tab = [1.4]; % Starting sparsity of the residual
    q_tab = 1/0.8;%[1.8,1.5,1.2];%[2,4,6,8]; Speed of decrease of the residual's sparsity
    
    resultTab = NaN*zeros(numel(P_REP_tab),numel(s_tab),numel(P_tab),numel(q_tab),N_FACTS_MAX-1,2);

    %for i = 4;
    for i=1:numel(P_REP_tab);
        %for j = 1;
        for j=1:numel(s_tab);
            for k = 1:numel(P_tab)
                for l = 1:numel(q_tab)

                    disp(['Run ' num2str(l + (k-1)*numel(q_tab) + (j-1)*numel(q_tab)*numel(P_tab) + (i-1)*numel(q_tab)*numel(P_tab)*numel(s_tab)) '/' num2str(numel(q_tab)*numel(P_tab)*numel(s_tab)*numel(P_REP_tab)) ])

                    %% Setting of the parameters
                    params.nfacts = N_FACTS;

                    % Constraints
                    params.cons = cell(2,N_FACTS-1);
                    params.cons{1,1} = {'splin',P_REP_tab(i),N_SAMP,DIM};
                    params.cons{2,1} = {'sp',DIM*DIM,DIM,DIM};
                    for m=2:N_FACTS-1
                        params.cons{1,m} = {'sp',s_tab(j)*DIM,DIM,DIM};
                        params.cons{2,m} = {'sp',P_tab(k)*(1/(q_tab(l)^(m-1)))*DIM^2,DIM,DIM};
                    end

                    % Number of iteration
                    params.niter1 = 200;
                    params.niter2 = 200;
                    params.verbose = 1;

                    [lambda, facts, errors] = hierarchical_fact(params);

                    %resultTab(i,j,k,l,:,:) = errors;
                    resultTab(i,j,k,l,1:N_FACTS-1,:) = errors;

                    err_rel = norm(X_fixed'-lambda*dvp(facts))/norm(X_fixed);
                    RC = nnzero_count(facts)/numel(X_fixed);

                    disp(['Relative error (operator norm): ' num2str(err_rel*100) '%'])
                    disp(['Relative complexity: ' num2str(RC*100) '%'])
                    
                    if (strcmp(demoType,'example'))
                        matfile = fullfile(pathname, 'output/M_user.mat');
                        save(matfile,'lambda','facts','errors');
                    end 
                    %save('results_current','resultTab');
                end
            end
        end
        % saving current resutlts in case of full demonstration
        if (strcmp(demoType,'full'))
            matfile = fullfile(pathname, 'output/results_current');
            save(matfile,'resultTab');
        end
    end
    heure = clock ;

    %save(['results_MEG_user'],'resultTab');
    matfile = fullfile(pathname, 'output/results_MEG_user');
    save(matfile,'resultTab');

    toc
end
