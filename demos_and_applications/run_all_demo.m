%% script run_demo_recomputeMEG
%  Script used to run all demo (source localisation, denoising) 
%  corresponding figures of the article [1]. 
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%
% [2]   A. Gramfort, M. Luessi, E. Larson, D. Engemann, D. Strohmeier, 
%	C. Brodbeck, L. Parkkonen, M. Hamalainen, MNE software for processing
%	MEG and EEG data <http://www.ncbi.nlm.nih.gov/pubmed/24161808>, 
%	NeuroImage, Volume 86, 1 February 2014, Pages 446-460, ISSN 1053-8119, 
%	[DOI] <http://dx.doi.org/10.1016/j.neuroimage.2013.10.027>
%%

% following parameter indicates that you do not use default precomputed data.  
precomputed_data='FALSE';

%% source localization
fprintf('Source localization demo')
MEG_fact('full');
Fig_SVD;
Fig_MEG;

BSL;
Fig_BSL;

%% image denoising
fprintf('Image denoising demo')
Denoising_dct('full');
Denoising_faust_ksvd('full');

display_results_denoising;

%% Fast Graph Fourier Transform
path = fileparts(mfilename('fullpath'));
cd path
fprintf('FGFT demo')
if ~exist('FFT_PG.mat','file')
    install_ack = input('\n\n Do you wish to download the pre-computed results (1.5 Go): ([y]/n)? ','s');
    if install_ack == 'y'
        fprintf('\n Download... \n')
        system('curl -o FGFT/precomputed_results.zip "ftp://ftp.irisa.fr/local/faust/precomputed_results.zip"');
        unzip('precomputed_results.zip','FGFT')
        addpath(genpath('precomputed_results'))
        fprintf('Demo running...')
        run_demo_FGFT_real_graphs
        fprintf('End of the demos \n')
    else
        fprintf('End of the demos \n')
    end
else
    fprintf('Demo running...')
    run_demo_FGFT_real_graphs
    fprintf('End of the demos \n')
end