This repository allows to reproduce the results of the paper:

Le Magoarou L., Gribonval R. and Tremblay N., "Approximate fast 
   graph Fourier transforms via multi-layer sparse approximations", 
   IEEE Transactions on Signal and Information Processing over 
   Networks,2017.	
   <https://hal.inria.fr/hal-01416110>

It is organized in 4 repositories : 

- methods: contains the functions implementing the algorithms of the paper
- experiments: contains the script used to generate the results used in the paper (can recompute the results saved in the precomputed_results repository)
- data: contains graph Laplacians used in the paper that are not in the GSP toolbox
- precomputed_results: contains the results used to generate the figures (can be recomputed with the scripts in the experiments repository)

To regenerate the figures and tables of the paper, you can either : 

- rerun the experiments of the paper by executing the scripts located in the "experiments" repository, the last part of these scripts will generate the figures or tables
- use the results saved in the "precomputed_results" repository (available at ftp://ftp.irisa.fr/local/faust/precomputed_results.zip, roughly 1.6 Gbytes), by running only the last part of the scripts located in "experiments"
