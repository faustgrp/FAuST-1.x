function [lambda, facts, D] = palm4LED_FFT(params)
%% Description hierarchical_fact_FFT 
%  Global optimization taking into account diagonalization using PALM.
%  [lambda, facts] = palm4LED(params) runs the PALM algorithm on the
%  specified set of signals (Algorithm 1 of [1]), returning the factors in
%  "facts" and the multiplicative scalar in "lambda".
%
%
%
%  Required fields in PARAMS:
%  --------------------------
%
%    'data' - Training data.
%      A matrix containing the training signals as its columns.
%
%    'nfacts' - Number of factors.
%      Specifies the desired number of factors.
%
%    'cons' - Constraint sets.
%      Specifies the constraint sets in which each factor should lie. It
%      should be a cell-array of size 1*nfacts, where the jth column sets
%      the constraints for the jth factor (starting from the left). cons(j)
%      should be itself a cell-array of size 1*4 taking this form for a
%      factor of size m*n:
%      {'constraint name', 'constraint parameter', m, n}
%
%    'niter' - Number of iterations.
%      Specifies the number of iterations to run.
%
%    'init_facts' - Initialization of "facts".
%      Specifies a starting point for the algorithm.
%
%
%
%  Optional fields in PARAMS:
%  --------------------------
%
%    'init_lambda' - Initialization of "lambda".
%      Specifies a starting point for the algorithm. The default value is 1
%
%    'init_D' - Initialization of "D".
%      Specifies a starting point for the algorithm. The default value is 
%      The identity.
%
%    'verbose' - Verbosity of the function. if verbose=1, the function
%      outputs the error at each iteration. if verbose=0, the function runs
%      in silent mode. The default value is 0.
%
%    'update_way' - Way in which the factors are updated. If update_way = 1
%      ,the factors are updated from right to left, and if update_way = 0,
%      the factors are updated from left to right. The default value is 0.
%
%    'stepsize' - Step size, the default value is 1e-6.
%
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Are there approximate Fast 
%   Fourier Transforms on graphs ?", ICASSP, 2016.
%	<https://hal.inria.fr/hal-01254108>
%%


%%%% Setting optional parameters values %%%%
if (isfield(params,'init_lambda'))
    init_lambda = params.init_lambda ;
else
    init_lambda = 1;
end

if (isfield(params,'verbose'))
    verbose = params.verbose ;
else
    verbose = 0;
end

if (isfield(params,'update_way'))
    update_way = params.update_way;
else
    update_way = 0;
end

if (isfield(params,'stepsize'))
    stepsize = params.stepsize;
else
    stepsize = 1e-6;
end

if params.nfacts ~= size(params.init_facts,2)
    error('Wrong initialization: params.nfacts and params.init_facts are in conflict')
end

%%%%%% Setting of the constraints %%%%%%%
handles_cell = cell(1,params.nfacts);
for ii=1:params.nfacts
    cons = params.cons{ii};
    if strcmp(cons{1},'sp')
        handles_cell{ii} = @(x) prox_sp(x,cons{2});
    elseif strcmp(cons{1},'spcol')
        handles_cell{ii} = @(x) prox_spcol(x,cons{2});
    elseif strcmp(cons{1},'splin')
        handles_cell{ii} = @(x) prox_splin(x,cons{2});
    elseif strcmp(cons{1},'normcol')
        handles_cell{ii} = @(x) prox_normcol(x,cons{2});
    elseif strcmp(cons{1},'splincol')
        handles_cell{ii} = @(x) prox_splincol(x,cons{2});
    elseif strcmp(cons{1},'l0pen')
        handles_cell{ii} = @(x) prox_l0pen(x,cons{2});
    elseif strcmp(cons{1},'l1pen')
        handles_cell{ii} = @(x) prox_l1pen(x,cons{2});
    elseif strcmp(cons{1},'const')
        handles_cell{ii} = @(x) cons{2};
    elseif strcmp(cons{1},'wav')
        handles_cell{ii} = @(x) prox_wav(x,cons{2});
    elseif strcmp(cons{1},'sppos')
        handles_cell{ii} = @(x) prox_sp_pos(x,cons{2});
    elseif strcmp(cons{1},'blkdiag')
        handles_cell{ii} = @(x) prox_blkdiag(x,cons{2});
    elseif strcmp(cons{1},'splin_test')
        handles_cell{ii} = @(x) prox_splin_test(x,cons{2});
    elseif strcmp(cons{1},'supp')
        handles_cell{ii} = @(x) prox_supp(x,cons{2});
    elseif strcmp(cons{1},'normlin')
        handles_cell{ii} = @(x) prox_normlin(x,cons{2});
    elseif strcmp(cons{1},'orth')
        handles_cell{ii} = @(x) prox_orth(x,cons{2});
    else
        error('The expressed type of constraint is not known')
    end
end

% Initialization
facts = params.init_facts;
for i = 1:numel(facts)
    facts{i} = sparse(facts{i});
end
lambda = init_lambda;
D = sparse(params.init_D);

%%%%%% Main Loop %%%%%%%
X = sparse(params.data);
if update_way
    maj = params.nfacts:-1:1;
else
    maj = 1:params.nfacts;
end

for i = 1:params.niter
    for j = maj
        if strcmp(params.cons{j}{1},'const')
            facts{j} = handles_cell{j}(facts{j});
        else
            L = facts(1:j-1);
            R = facts(j+1:end);
            
            grad_S = grad_FFTgraph_S(L,facts{j},R,params.data,D,lambda);
            c_S = 1/stepsize;%1e7; %1e6 for rand_sensor_1024
            facts{j} = handles_cell{j}(facts{j} - (1/c_S)*grad_S);
            
        end
    end
    Xhat =[facts,{D},trans_FAUST(facts)];
    %Xhat = temp*D*temp';
    %lambda = sqrt(trace(X'*Xhat)/trace(Xhat'*Xhat));
	lambda = sqrt(trace(X'*dvp(Xhat))/trace(dvp([trans_FAUST(Xhat),Xhat])));
    Uhat = [{lambda*facts{1}},facts(2:end)];
    grad_D = grad_FFTgraph_D(Uhat,X,D);
    c_D = 1/stepsize;%1e7; %1e6 for rand_sensor_1024
    D = diag(diag(D - (1/c_D)*grad_D));
    if verbose %&& i==1 || (mod(i,20)==0)
        disp(['Iter ' num2str(i) ', RE=' num2str(norm(X - dvp([Uhat,D,trans_FAUST(Uhat)]),'fro')^2/norm(X,'fro')^2) ])
    end
end
end