function grad = grad_FFTgraph_D(Uhat,X,D)
%% Description grad_FFTgraph_D 
% Computation of the gradient with respect to the diagonal matrix D.
%  grad = grad_FFTgraph_S(L,S,R,X,D,lambda) computes the gradient "grad" of
%  H(L,S,R,D,lambda) = 1/4|| X - Uhat*D*Uhat' ||.
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Are there approximate Fast 
%   Fourier Transforms on graphs ?", ICASSP, 2016.
%	<https://hal.inria.fr/hal-01254108>
%%

% Compute the gradient
Uhat = dvp(Uhat);
grad = 0.5*Uhat'*(Uhat*D*Uhat' - X)*Uhat;

%Uhat = [L,{lambda*S},R];
% UhatDUhat = [Uhat,{D},trans_FAUST(Uhat)];
% inbrackets = dvp(UhatDUhat) - X;
% left = 0.5 * mult_FAUST(trans_FAUST(Uhat),inbrackets);
% grad = left*dvp(Uhat);

end