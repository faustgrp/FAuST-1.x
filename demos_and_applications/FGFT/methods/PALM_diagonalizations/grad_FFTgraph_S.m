function grad = grad_FFTgraph_S(L,S,R,X,D,lambda)
%% Description grad_FFTgraph_S  
%Computation of the gradient with respect to a sparse factor S.
%  grad = grad_FFTgraph_S(L,S,R,X,D,lambda) computes the gradient grad of
%  H(L,S,R,D,lambda) = 1/4|| X - lambda^2*L*S*R*D*R^T*S^T*L^T ||.
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Are there approximate Fast 
%   Fourier Transforms on graphs ?", ICASSP, 2016.
%	<https://hal.inria.fr/hal-01254108>
%%


% Compute the gradient
L = dvp(L); R=dvp(R);
Uhat = lambda*L*S*R;
grad = lambda*L'*(Uhat*D*Uhat' - X)*Uhat*D*R';

%LSRDRt = [L,{S},R,{D},trans_FAUST(R)];
%LSRDRtStLt = [LSRDRt, {S'}, trans_FAUST(L)];

%inbrackets = lambda^2*dvp(LSRDRtStLt) - X;
%left = lambda * mult_FAUST(tans_FAUST(L),inbrackets);

%grad = left*lambda*dvp(LSRDRt);

% Uhat = [L,{lambda*S},R];
% UhatDUhat = [Uhat,{D},trans_FAUST(Uhat)];
% inbrackets = dvp(UhatDUhat) - X;
% left = lambda * mult_FAUST(trans_FAUST(L),inbrackets);
% grad = left*dvp([Uhat,{D},trans_FAUST(R)]);

end