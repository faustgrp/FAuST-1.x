%% Description experiment_givens_parall_rand_sensor
%  Evaluation of approximate FFTs on random sensor graphs.
%
%  This script allows to reproduce the Table IV of section V of [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%

%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L., Gribonval R. and Tremblay N., "Approximate fast 
%   graph Fourier transforms via multi-layer sparse approximations", 
%   submitted to IEEE Transactions on Signal and Information Processing
%   over Networks.
%	<https://hal.inria.fr/hal-01416110>
%%

T_start = tic;

DIMs = [64, 128, 256, 512, 1024, 2048, 4096, 8192]; % considered sizes
t_diag_dense =zeros(1,numel(DIMs));
t_diag_faust =zeros(1,numel(DIMs));
t_mult_dense =zeros(1,numel(DIMs));
t_mult_faust =zeros(1,numel(DIMs));
RCGs =zeros(1,numel(DIMs));
time_gain =zeros(1,numel(DIMs));
err_diag_faust = zeros(1,numel(DIMs));


for i=1:numel(DIMs)
    DIM = DIMs(i);
    disp(['DIM = ' num2str(DIM)])
    J = 2*DIM*log2(DIM);
    G = gsp_sensor(DIM); % generate the ransom sensors graph
    tic
    G = gsp_compute_fourier_basis(G);
    t_diag_dense(i) = toc;
    U = full(G.U);
    RCGs(i) = DIM^2/(4*J);
    
    
    %%% Greedy diagonalization (algo of fig.3)
    tic
    [facts,D,err,L,coord_choices] = diagonalization_givens_parall(G.L,J,DIM/2);
    t_diag_faust(i) = toc;
    err_diag_faust(i)=sqrt(err(end));
    
    
    %%% Measuring the actual time gain
    Ntrials = 100;
    Nfacts = numel(facts);
    t_faust = zeros(1,Ntrials);
    t_dense = zeros(1,Ntrials);
    for t=1:Ntrials
        x=randn(DIM,1);
        
        tic
        y_faust=x;
        for j=1:Nfacts
            y_faust=facts{j}'*y_faust;
        end
        t_faust(t) = toc;
        
        tic
        y_dense=U'*x;
        t_dense(t) = toc;
        
    end
    t_mult_faust(i) = mean(t_faust);
    t_mult_dense(i) = mean(t_dense);
    time_gain(i) = t_mult_dense(i)/t_mult_faust(i);
end

T_total = toc(T_start);

%%% Save the results
ResTab_givens_parall = [t_diag_dense ;
    t_diag_faust ;
    t_mult_dense ;
    t_mult_faust ;
    RCGs ;
    time_gain ;
    err_diag_faust ];
heure = clock ;
save(['givens_parall_rand_sensor' date '-' num2str(heure(4)) '-' num2str(heure(5))  ],'ResTab_givens_parall');

%% Loading precomputed results (if you do not want to recompute everything)
load('givens_parall_rand_sensor')

%% Generating table IV
table = ResTab_givens_parall(5:7,:);