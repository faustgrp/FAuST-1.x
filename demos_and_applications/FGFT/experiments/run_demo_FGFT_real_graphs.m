%% Description demo_FGFT_real_graphs
%  Demonstration of approximate FFTs on real world sensor graphs.
%
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%

%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L., Gribonval R. and Tremblay N., "Approximate fast 
%   graph Fourier transforms via multi-layer sparse approximations", 
%   submitted to IEEE Transactions on Signal and Information Processing
%   over Networks.
%	<https://hal.inria.fr/hal-01416110>
%%

%% Building the approximate FGFTs
load('FFT_PG') % Power grid graph

%%% Building the UtUs
n = size(Lap,1);
J = 6*n*round(log2(n));
RCG = n^2/(4*J);
Nfacts_tab = [round(numel(facts)/6),round(numel(facts)/3),numel(facts)];
tic
UTU=U;
STLS = Lap;
for j=1:Nfacts_tab(1)
    UTU=facts{j}'*UTU;
    STLS = facts{j}'*STLS*facts{j};
end
spec_hat = diag(STLS);
[~,Ispec] = sort(spec_hat);
Perm_1 = Ispec;
D_hat_1 = diag(spec_hat(Ispec));
UTU_1=UTU(Perm_1,:);

for j=Nfacts_tab(1)+1:Nfacts_tab(2)
    UTU=facts{j}'*UTU;
    STLS = facts{j}'*STLS*facts{j};
end
spec_hat = diag(STLS);
[~,Ispec] = sort(spec_hat);
Perm_2 = Ispec;
D_hat_2 = diag(spec_hat(Ispec));
UTU_2=UTU(Perm_2,:);

for j=Nfacts_tab(2)+1:Nfacts_tab(3)
    UTU=facts{j}'*UTU;
end
UTU_3=UTU;

%%% Measuring the actual time gain
Ntrials = 100;
DIM=n;
t_faust = zeros(3,Ntrials);
t_dense = zeros(1,Ntrials);
for t=1:Ntrials
    x=randn(DIM,1);
    
    tic
    y_dense=U'*x;
    t_dense(t) = toc;
    
    tic
    y_faust=x;
    for j=1:Nfacts_tab(1)
        y_faust=facts{j}'*y_faust;
    end
    t_faust(1,t) = toc;
    
    
    tic
    y_faust=x;
    for j=1:Nfacts_tab(2)
        y_faust=facts{j}'*y_faust;
    end
    t_faust(2,t) = toc;
    
    
    tic
    y_faust=x;
    for j=1:Nfacts_tab(3)
        y_faust=facts{j}'*y_faust;
    end
    t_faust(3,t) = toc; 
   
end
t_mult_faust = mean(t_faust,2);
t_mult_dense = mean(t_dense);
time_gain = 1./(t_mult_faust/t_mult_dense);      

%% Generating the figure

%%% Generating the test signal in the Fourier domain
signal_test = [zeros(1,round(n/8)) ...
               ones(1,round(n/8)).*randn(1,round(n/8)) ...
               zeros(1,round(n/8)) ...
               ((1:round(n/8))/round(n/8)).*randn(1,round(n/8)) ...
               zeros(1,round(n/8)) ...
               (sin((1:round(n/8))/(round(n/8)/pi))).*randn(1,round(n/8)) ...
               zeros(1,n-6*round(n/8)) ...
               ];%sin((1:300)/(300/pi))
           
signal_test_1 = UTU_1*signal_test';
signal_test_2 = UTU_2*signal_test';
signal_test_3 = UTU_3*signal_test';


cols = get(gca,'ColorOrder');
figure('color','white')
subplot(4,1,1)
plot(abs(signal_test),'color',cols(4,:));
title('Exact spectrum of the signal')
subplot(4,1,2)
plot(abs(signal_test_3),'color',cols(3,:));
title(['Approximate spectrum of the signal, RCG = ' num2str(RCG,'%6.1f') ', Time gain = ' num2str(time_gain(3),'%6.1f')])
subplot(4,1,3)
plot(abs(signal_test_2),'color',cols(2,:));
title(['Approximate spectrum of the signal, RCG = ' num2str(RCG*3,'%6.1f') ', Time gain = ' num2str(time_gain(2),'%6.1f')])
subplot(4,1,4)
plot(abs(signal_test_1),'color',cols(1,:));
title(['Approximate spectrum of the signal, RCG = ' num2str(RCG*6,'%6.1f') ', Time gain = ' num2str(time_gain(1),'%6.1f')])
