%% Description experiment_comparison_PALM_givens
%  Comparison of factorizations and diagonalization methods on random
%  graphs.
%
%  This script allows to reproduce the Table II of section IV of [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%

%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L., Gribonval R. and Tremblay N., "Approximate fast 
%   graph Fourier transforms via multi-layer sparse approximations", 
%   submitted to IEEE Transactions on Signal and Information Processing
%   over Networks.
%	<https://hal.inria.fr/hal-01416110>
%%

%% ==================== Global parameters =========================
%%%
DIMtab = [128,256,512,1024]; % Dimension of the graphs (number of nodes)
Nrun=1;

resTab = zeros(4,4,5,Nrun,numel(DIMtab)); % technique,mesure,graph,run,size

%%%
%% ==================== Main loop =========================
%%%
for k = 1:numel(DIMtab)
    DIM = DIMtab(k);
    %disp(DIM)
    for n=1:Nrun
        %disp(n)
        for l = 1:6
            disp(['k=' num2str(k) ', n=' num2str(n) ', l=' num2str(l)])
            
            %%%
            %% ==================== Graph generation =========================
            %%%
            if l==1
                %disp(['l = ' num2str(l)])
                G = gsp_erdos_renyi(DIM,0.1);
            elseif l==2
                %disp(['l = ' num2str(l)])
                G = gsp_community(DIM);
            elseif l==3
                %disp(['l = ' num2str(l)])
                G = gsp_sensor(DIM);
            elseif l==4
                %disp(['l = ' num2str(l)])
                G = gsp_path(DIM);
            elseif l==5
                %disp(['l = ' num2str(l)])
                G = gsp_random_ring(DIM);
            elseif l==6
                %disp(['l = ' num2str(l)])
                G = gsp_ring(DIM);
            end
            
            %G = gsp_comet(DIM,20);
            %G=gsp_david_sensor_network(DIM);
            %G=gsp_full_connected(DIM);
            %G=gsp_random_regular(DIM);
            %G=gsp_random_ring(DIM);
            %G=gsp_ring(DIM);
            %G=gsp_swiss_roll(DIM);
            %G = gsp_path(DIM);
            %G = gsp_minnesota(1); DIM = G.N;
            %figure;gsp_plot_graph(G);
            
            Lap = full(G.L);
            G = gsp_compute_fourier_basis(G);
%             U = full(G.U);
%             D = diag(G.e);
            [U,D]=eig(Lap);
        
            %%%
            %% ==================== PALM factorization =========================
            %%%
            disp('Factorisation PALM de la matrice de Fourier')
            % Setting of the parameters
            N_FACTS = round(log2(DIM))-3; % Desired number of factors
            over_sp = 1.5; % Sparsity overhead
            dec_fact = 0.5; % Decrease of the residual sparsity
            params.data = U;
            params.Lap = Lap;
            params.init_D = D;
            params.nfacts = N_FACTS;
            params.fact_side = 1;
            
            
            params.cons = cell(2,N_FACTS-1);
            for j=1:N_FACTS-1
                params.cons{1,j} = {'sp',dec_fact^j*DIM^2*over_sp,DIM,DIM};
                params.cons{2,j} = {'sp',2*DIM*over_sp,DIM,DIM};
            end
            
            % Number of iterations
            params.niter1 = 50;
            params.niter2 = 100;
            params.verbose=0;
            
            tic
            [lambda, facts, errors] = hierarchical_fact(params);
            t_factorization = toc;
            
            complexity_global=0;
            for j=1:N_FACTS
                complexity_global = complexity_global + nnz(facts{j});
            end
            RC_PALM = complexity_global/nnz(U);
            disp(['RCG_PALM = ' num2str(1/RC_PALM)])
            
            Uhat_PALM = lambda*dvp(facts);
            
            resTab(1,1,l,n,k) = norm(U-Uhat_PALM,'fro')/norm(U,'fro');
            resTab(1,2,l,n,k) = norm(Uhat_PALM'*Lap*Uhat_PALM - diag(diag(Uhat_PALM'*Lap*Uhat_PALM)),'fro')/norm(Lap,'fro');
            resTab(1,3,l,n,k) = t_factorization;
            resTab(1,4,l,n,k) = 1/RC_PALM;
            disp(['error1_PALM = ' num2str(resTab(1,1,l,n,k))])
            disp(['error2_PALM = ' num2str(resTab(1,2,l,n,k))])
            
            
            %%%
            %% ==================== PALM diagonalization =========================
            %%%
            disp('Diagonalisation PALM')
            tic
            [lambda, facts, Dhat, errors] = hierarchical_fact_FFT(params);
            t_diag_PALM = toc;
            
            complexity_global=0;
            for j=1:N_FACTS
                complexity_global = complexity_global + nnz(facts{j});
            end
            RC_PALM_diag = complexity_global/nnz(U);
            disp(['RCG_PALM_diag = ' num2str(1/RC_PALM_diag)])
            
            Uhat_PALM_diag = lambda*dvp(facts);
            
            resTab(2,1,l,n,k) = norm(U-Uhat_PALM_diag,'fro')/norm(U,'fro');
            resTab(2,2,l,n,k) = norm(Uhat_PALM_diag'*Lap*Uhat_PALM_diag - diag(diag(Uhat_PALM_diag'*Lap*Uhat_PALM_diag)),'fro')/norm(Lap,'fro');
            resTab(2,3,l,n,k) = t_diag_PALM;
            resTab(2,4,l,n,k) = 1/RC_PALM_diag;
            disp(['error1_PALM = ' num2str(resTab(2,1,l,n,k))])
            disp(['error2_PALM = ' num2str(resTab(2,2,l,n,k))])
            
            %%%
            %% ==================== Givens diagonalization =========================
            %%%
            disp('Diagonalisation du Laplacien')
            J=round(RC_PALM*nnz(U)/4);
            tic
            [facts_givens,D,err,L,choices] = diagonalization_givens(Lap,J);
            t_diagonalization = toc;
            
            %fourier_diag = apply_multiple_givens_left(flipud(facts_givens),eye(DIM));
            fourier_diag = eye(DIM);
            for j=1:numel(facts_givens)
                fourier_diag=facts_givens{j}'*fourier_diag;
            end
            
            fourier_diag_full = full(fourier_diag');
            spectrum = diag(D);
            [~,I] = sort(spectrum);
            Uhat_givens = fourier_diag_full(:,I);
            for i=1:size(Uhat_givens,2)
                Uhat_givens(:,i) = Uhat_givens(:,i).*sign(Uhat_givens(:,i)'*U(:,i));
            end
            
            RC_givens = 4*J/nnz(U);
            disp(['RCG_givens = ' num2str(1/RC_givens)])
            
            resTab(3,1,l,n,k) = norm(U-Uhat_givens,'fro')/norm(U,'fro');
            resTab(3,2,l,n,k) = norm(Uhat_givens'*Lap*Uhat_givens - diag(diag(Uhat_givens'*Lap*Uhat_givens)),'fro')/norm(Lap,'fro');
            resTab(3,3,l,n,k) = t_diagonalization;
            resTab(3,4,l,n,k) = 1/RC_givens;
            disp(['error1_givens = ' num2str(resTab(3,1,l,n,k))])
            disp(['error2_givens = ' num2str(resTab(3,2,l,n,k))])
            
            %%% Display of the results
            %             figure('color','w')
            %             imagesc(U'*Uhat_PALM)
            %             caxis([-1,1])
            %             figure('color','w')
            %             imagesc(U'*Uhat_givens)
            %             caxis([-1,1])
            
            
            %%%
            %% ==================== Givens parallel diagonalization =========================
            %%%
            disp('Diagonalisation parallele du Laplacien')
            J=round(RC_PALM*nnz(U)/4);
            tic
            [facts_givens_parall,D,err,L,coord_choices] = diagonalization_givens_parall(Lap,J,DIM/2);
            t_diagonalization_parall = toc;
            
            Uhat_givens_parall=eye(DIM);
            STLS = Lap;
            for j=1:numel(facts_givens_parall)
                Uhat_givens_parall=facts_givens_parall{j}'*Uhat_givens_parall;
            end
            
            Uhat_givens_parall = Uhat_givens_parall';
            
            for i=1:size(Uhat_givens_parall,2)
                Uhat_givens_parall(:,i) = Uhat_givens_parall(:,i).*sign(Uhat_givens_parall(:,i)'*U(:,i));
            end
            
            RC_givens_parall = 4*J/nnz(U);
            disp(['RCG_givens_parall = ' num2str(1/RC_givens_parall)])
            
            resTab(4,1,l,n,k) = norm(U-Uhat_givens_parall,'fro')/norm(U,'fro');
            resTab(4,2,l,n,k) = norm(Uhat_givens_parall'*Lap*Uhat_givens_parall - diag(diag(Uhat_givens_parall'*Lap*Uhat_givens_parall)),'fro')/norm(Lap,'fro');
            resTab(4,3,l,n,k) = t_diagonalization_parall;
            resTab(4,4,l,n,k) = 1/RC_givens_parall;
            disp(['error1_givens = ' num2str(resTab(4,1,l,n,k))])
            disp(['error2_givens = ' num2str(resTab(4,2,l,n,k))])
            
            
            
            save('comp_PALM_givens_CURRENT','resTab');
        end
    end
    save(['comp_PALM_givens_CURRENT_upto_' num2str(DIMtab(k))],'resTab');
end

heure = clock;
save(['comparison_PALM_givens' date '_' num2str(heure(4)) '_' num2str(heure(5))],'resTab');

%% Loading precomputed results (if you do not want to recompute everything)
%load('comparison_PALM_givens')

%% Building table II

% n=128
input.data = reshape(permute(mean(resTab(:,1:3,1:6,:,1),4),[2,1,3]),[3,24,1]);
input.dataFormat = {'%.2f'};
latex_tab_128 = latexTable(input);

% n=256
input.data = reshape(permute(mean(resTab(:,1:3,1:6,:,2),4),[2,1,3]),[3,24,1]);
input.dataFormat = {'%.2f'};
latex_tab_256 = latexTable(input);

% n=512
input.data = reshape(permute(mean(resTab(:,1:3,1:6,:,3),4),[2,1,3]),[3,24,1]);
input.dataFormat = {'%.2f'};
latex_tab_512 = latexTable(input);

% n=1024
input.data = reshape(permute(mean(resTab(:,1:3,1:6,:,4),4),[2,1,3]),[3,24,1]);
input.dataFormat = {'%.2f'};
latex_tab_1024 = latexTable(input);



