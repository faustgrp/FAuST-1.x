%% Description experiment_filtering_minnesota
%  Comparison of approximate FFTs and polynomial approximation methods on a
%  denoising task on the Minnesota road graph.
%
%  This script allows to reproduce the Table V of section VI of [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%

%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L., Gribonval R. and Tremblay N., "Approximate fast 
%   graph Fourier transforms via multi-layer sparse approximations", 
%   submitted to IEEE Transactions on Signal and Information Processing
%   over Networks.
%	<https://hal.inria.fr/hal-01416110>
%%

%% ---------------------- Parameters and graph generation
Ntrials = 100; % Number of random signals generated
noise_std_tab = [2e-1, 2.5e-1, 3e-1, 4e-1, 5e-1, 6e-1]; % Noise levels
reg_param = 3e0; % Regularization parameter for the filtering
resTab = zeros(13,numel(noise_std_tab),2,Ntrials); % method,noiselevel,type of filter,trial
poly_order_tab=[14 28 40]; % Order of the polynomial approximation
cutoff = 300; % Index of cutoff frequency
G = gsp_minnesota();
Lap = full(G.L);
G = gsp_compute_fourier_basis(G);
U = full(G.U);
D = diag(G.e);
Dtrue=D;
DIM = size(D,1);
G = gsp_estimate_lmax(G);

%% ---------------------- Loading of the approximate Fourier matrices

% PALM factorization
load 'precomputed_results/minnesota_PALM.mat'
facts_facto = facts;
lambda_facto = lambda;
D_palm_facto = Dhat;
U_palm_facto = lambda_facto*dvp(facts_facto);
sparsity_tot =0;
for i=1:numel(facts_facto)
    sparsity_tot = sparsity_tot + nnz(facts_facto{i});
end
RCG_palm_facto = DIM^2/sparsity_tot;

% PALM diagonalization
load 'precomputed_results/minnesota_PALM_d.mat'
facts_diago = facts;
lambda_diago = lambda;
D_palm_diago = Dhat;
U_palm_diago = lambda_diago*dvp(facts_diago);
sparsity_tot =0;
for i=1:numel(facts_diago)
    sparsity_tot = sparsity_tot + nnz(facts_diago{i});
end
RCG_palm_diago = DIM^2/sparsity_tot;

% Givens diagonalization
fourier_tab=cell(1,4);
D_tab = cell(1,4);
%load 'givens_minnesota_upto_250000.mat'
load 'precomputed_results/minnesota_Givens.mat'
U_givens_tab = fourier_tab(1:3);
D_givens_tab = D_tab(1:3);


% Givens parallel diagonalization
load 'precomputed_results/minnesota_Givens_parall.mat'
Nfacts_tab = [38,76,114];
U_hat=eye(DIM);
STLS = Lap;
for j=1:Nfacts_tab(1)
    U_hat=U_hat*facts{j};
    STLS = facts{j}'*STLS*facts{j};
end
spec_hat = diag(STLS);
[~,Ispec] = sort(spec_hat);
Perm_1 = Ispec;
D_hat_1 = diag(spec_hat(Ispec));
U_givens_parall_50000=U_hat(:,Perm_1);

for j=Nfacts_tab(1)+1:Nfacts_tab(2)
    U_hat=U_hat*facts{j};
    STLS = facts{j}'*STLS*facts{j};
end
spec_hat = diag(STLS);
[~,Ispec] = sort(spec_hat);
Perm_2 = Ispec;
D_hat_2 = diag(spec_hat(Ispec));
U_givens_parall_100000=U_hat(:,Perm_2);

for j=Nfacts_tab(2)+1:Nfacts_tab(3)
    U_hat=U_hat*facts{j};
    STLS = facts{j}'*STLS*facts{j};
end
spec_hat = diag(STLS);
[~,Ispec] = sort(spec_hat);
Perm_3 = Ispec;
D_hat_3 = diag(spec_hat(Ispec));
U_givens_parall_150000=U_hat(:,Perm_3);

U_givens_parall_tab = {U_givens_parall_50000,U_givens_parall_100000,U_givens_parall_150000};
D_givens_parall_tab = {D_hat_1,D_hat_2,D_hat_3};


U_tab = {U,U_palm_diago,U_palm_facto,U_givens_tab{:},U_givens_parall_tab{:}};
D_tab = {D,D_palm_diago,D_palm_facto,D_givens_tab{:},D_givens_parall_tab{:}};


%% ---------------------- Main loop
for noise_idx=1:numel(noise_std_tab)
    noise_std = noise_std_tab(noise_idx);
    disp(['noise_idx=' num2str(noise_idx)]);
    for trial=1:Ntrials
        disp(['trial ' num2str(trial)]);
        %f_spec = randn(DIM,1)./exp(G.e); % Spectrum of the random signal
        f_spec = randn(DIM,1).*[ones(cutoff,1); zeros(DIM-cutoff,1)]; % Spectrum of the random signal
        f = G.U*f_spec; % Random signal in the node domain
        param.show_edges=1;
        
        f_noisy = f + noise_std*randn(size(f)); % Adding the noise
        SNR_noisy = 20*log10(norm(f)/norm(f-f_noisy));
        resTab(1,noise_idx,1,trial) = SNR_noisy;
        resTab(1,noise_idx,2,trial) = SNR_noisy;
        
        
        %%%%%% Denoising using approximate Fourier transforms %%%%%%
        for four_idx = 1:numel(U_tab)
            U_used = U_tab{four_idx};
            D_used = D_tab{four_idx};
            
            f_denoised_exp = U_used*diag(exp(-diag(D_used)))*(U_used'*f_noisy);
            f_denoised_ideal = U_used*(diag(diag(D_used)<=Dtrue(cutoff,cutoff))*(U_used'*f_noisy));
            SNR_exp = 20*log10(norm(f)/norm(f-f_denoised_exp));
            SNR_ideal = 20*log10(norm(f)/norm(f-f_denoised_ideal));
            resTab(four_idx+1,noise_idx,1,trial) = SNR_exp;
            resTab(four_idx+1,noise_idx,2,trial) = SNR_ideal;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %%%%%% Denoising using polynomial approximations %%%%%%
        %g_exp=@(x) 1./(1+reg_param*x);
        g_exp=@(x) exp(-x);
        g_ideal=@(x) x<=Dtrue(cutoff,cutoff);
        f_denoised_cheby = cell(1,3);
        SNR_denoised_cheby = zeros(1,3);
        RCG_cheby = zeros(1,3);
        for ind_order = 1:numel(poly_order_tab)
            coeffs_exp = gsp_cheby_coeff(G, g_exp, poly_order_tab(ind_order));
            f_denoised = gsp_cheby_op(G, coeffs_exp, f_noisy);
            SNR_denoised = 20*log10(norm(f)/norm(f-f_denoised));
            resTab(10+ind_order,noise_idx,1,trial) = SNR_denoised;
            
            coeffs_ideal = gsp_cheby_coeff(G, g_ideal, poly_order_tab(ind_order));
            f_denoised = gsp_cheby_op(G, coeffs_ideal, f_noisy);
            SNR_denoised = 20*log10(norm(f)/norm(f-f_denoised));
            resTab(10+ind_order,noise_idx,2,trial) = SNR_denoised;
            
            RCG_cheby(ind_order) = DIM^2./(poly_order_tab(ind_order)*(2*DIM+nnz(G.L)));
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    end
end

% Showing the results (figure 6 of the paper)

U_used = U_tab{1};
D_used = D_tab{1};
f_denoised_U = U_used*(diag(diag(D_used)<=Dtrue(cutoff,cutoff))*(U_used'*f_noisy));
SNR_U = 20*log10(norm(f)/norm(f-f_denoised_U));

U_used = U_tab{11};
D_used = D_tab{11};
f_denoised_approx = U_used*(diag(diag(D_used)<=Dtrue(cutoff,cutoff))*(U_used'*f_noisy));
SNR_approx = 20*log10(norm(f)/norm(f-f_denoised_approx));

figure('color','w')
im=subplot_tight(2,2,1,0.05);gsp_plot_signal(G,f,param); title('Clean signal','interpreter','latex','fontsize',16); axis square
cax = caxis(im);
subplot_tight(2,2,2,0.05);gsp_plot_signal(G,f_noisy,param);title(['Noisy signal, SNR=' num2str(SNR_noisy,'%4.2f') 'dB'],'interpreter','latex','fontsize',16); axis square; caxis(cax);
subplot_tight(2,2,3,0.05);gsp_plot_signal(G,f_denoised_U,param);title(['Filtered signal using $\mathbf{U}$, SNR=' num2str(SNR_U,'%4.2f') 'dB'],'interpreter','latex','fontsize',16); axis square; caxis(cax);
subplot_tight(2,2,4,0.05);gsp_plot_signal(G,f_denoised_approx,param);title(['Filtered signal using $\hat{\mathbf{U}}_{Givens //}$, SNR=' num2str(SNR_approx,'%4.2f') 'dB'],'interpreter','latex','fontsize',16); axis square; caxis(cax);


% Saving the results
heure = clock;
save(['minnesota_filtering' date '_' num2str(heure(4)) '_' num2str(heure(5))],'resTab','noise_std_tab');

%% Loading precomputed results (uncomment if you do not want to recompute everything)
%load('minnesota_filtering')

%% Building table V
input.data = mean(resTab(:,:,1,:),4);
input.dataFormat = {'%.2f'};
input.tableColLabels = cell(1,numel(noise_std_tab));
for ii=1:numel(noise_std_tab)
    input.tableColLabels{ii} = ['$\sigma=' num2str(noise_std_tab(ii)) '$'];
end
input.tableRowLabels = {'Noisy','denoised with U','PALM diagonalization','PALM factorization','Givens 50000','Givens 100000','Givens 150000','parallel Givens 50000','parallel Givens 100000','parallel Givens 150000','poly 1','poly 2','poly 3'};
latex_exp = latexTable(input);

input.data = mean(resTab(:,:,2,:),4);
input.dataFormat = {'%.2f'};
input.tableColLabels = cell(1,numel(noise_std_tab));
for ii=1:numel(noise_std_tab)
    input.tableColLabels{ii} = ['$\sigma=' num2str(noise_std_tab(ii)) '$'];
end
input.tableRowLabels = {'Noisy','denoised with U','PALM diagonalization','PALM factorization','Givens 50000','Givens 100000','Givens 150000','parallel Givens 50000','parallel Givens 100000','parallel Givens 150000','poly 1','poly 2','poly 3'};
latex_ideal = latexTable(input);