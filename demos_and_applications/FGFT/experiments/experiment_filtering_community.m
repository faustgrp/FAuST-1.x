%% Description experiment_filtering_community
%  Comparison of approximate FFTs and polynomial approximation methods on
%  random community graphs.
%
%  This script allows to reproduce the Table VI of section VI of [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%

%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.lemagoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L., Gribonval R. and Tremblay N., "Approximate fast 
%   graph Fourier transforms via multi-layer sparse approximations", 
%   submitted to IEEE Transactions on Signal and Information Processing
%   over Networks.
%	<https://hal.inria.fr/hal-01416110>
%%

% Generation of the graph
G = gsp_community(2048);
Lap = full(G.L);
G = gsp_compute_fourier_basis(G);
U = full(G.U);
D = diag(G.e);
Dtrue=D;
DIM = size(D,1);

%%% Greedy diagonalizations (algo of fig.3)
J = 6*round(DIM*log(DIM));

[facts,Dhat3,err,L,coord_choices] = diagonalization_givens_parall(G.L,J,DIM/2);

%%% Computation of the approximate Fourier matrices
Nfacts_tab = [round(numel(facts)/3),round(numel(facts)/1.5),numel(facts)];
nnzeros = 0;
Uhat = eye(DIM);
STLS = Lap;
for i=1:Nfacts_tab(1)
    Uhat = Uhat*facts{i};
    STLS = facts{i}'*STLS*facts{i};
    nnzeros=nnzeros + nnz(facts{i});
end
RCG1 = DIM^2/nnzeros;
spec_hat = diag(STLS);
[~,Ispec] = sort(spec_hat);
Perm_1 = Ispec;
Dhat1 = diag(spec_hat(Ispec));
Uhat1=Uhat(:,Perm_1);

for i=Nfacts_tab(1)+1:Nfacts_tab(2)
    Uhat = Uhat*facts{i};
    STLS = facts{i}'*STLS*facts{i};
    nnzeros=nnzeros + nnz(facts{i});
end
RCG2 = DIM^2/nnzeros;
spec_hat = diag(STLS);
[~,Ispec] = sort(spec_hat);
Perm_2 = Ispec;
Dhat2 = diag(spec_hat(Ispec));
Uhat2=Uhat(:,Perm_2);

for i=Nfacts_tab(2)+1:Nfacts_tab(3)
    Uhat = Uhat*facts{i};
    STLS = facts{i}'*STLS*facts{i};
    nnzeros=nnzeros + nnz(facts{i});
end
RCG3 = DIM^2/nnzeros;
Uhat3=Uhat;

U_tab = {Uhat1,Uhat2,Uhat3};
D_tab = {Dhat1,Dhat2,Dhat3};
poly_order1 = round((DIM^2/RCG1 - DIM)/(nnz(G.L) + DIM));
poly_order2 = round((DIM^2/RCG2 - DIM)/(nnz(G.L) + DIM));
poly_order3 = round((DIM^2/RCG3 - DIM)/(nnz(G.L) + DIM));
poly_order_tab = [poly_order1,poly_order2,poly_order3];
RCG_givens = [RCG1,RCG2,RCG3];


%% Comparison of the filtering matrices
%------------ Ideal low-pass 1
cutoff = 7;
H = U*diag(diag(Dtrue)<=Dtrue(cutoff,cutoff))*U';


err_givens_lp = zeros(1,numel(U_tab));
for ind_fourier = 1:numel(U_tab)
    Uhat = U_tab{ind_fourier};
    Dhat = D_tab{ind_fourier};
    H_givens = Uhat*diag(diag(Dhat)<=Dhat(cutoff,cutoff))*Uhat';
    err_givens_lp(ind_fourier) = norm(H-H_givens,'fro')/norm(H,'fro');
end


err_cheby_lp = zeros(1,3);
RCG_cheby = zeros(1,3);
g=@(x) x<=Dtrue(cutoff,cutoff);
for ind_order = 1:numel(poly_order_tab)
    coeffs = gsp_cheby_coeff(G, g, poly_order_tab(ind_order));
    H_cheby = gsp_cheby_op(G, coeffs, eye(DIM));
    RCG_cheby(ind_order) = DIM^2./(poly_order_tab(ind_order)*(2*DIM+nnz(G.L)));
    err_cheby_lp(ind_order) = norm(H-H_cheby,'fro')/norm(H,'fro');
end

%------------ Ideal low-pass 2
cutoff = 1000;
H = U*diag(diag(Dtrue)<=Dtrue(cutoff,cutoff))*U';


err_givens_lp2 = zeros(1,numel(U_tab));
for ind_fourier = 1:numel(U_tab)
    Uhat = U_tab{ind_fourier};
    Dhat = D_tab{ind_fourier};
    H_givens = Uhat*diag(diag(Dhat)<=Dhat(cutoff,cutoff))*Uhat';
    err_givens_lp2(ind_fourier) = norm(H-H_givens,'fro')/norm(H,'fro');
end


err_cheby_lp2 = zeros(1,3);
RCG_cheby = zeros(1,3);
g=@(x) x<=Dtrue(cutoff,cutoff);
for ind_order = 1:numel(poly_order_tab)
    coeffs = gsp_cheby_coeff(G, g, poly_order_tab(ind_order));
    H_cheby = gsp_cheby_op(G, coeffs, eye(DIM));
    RCG_cheby(ind_order) = DIM^2./(poly_order_tab(ind_order)*(2*DIM+nnz(G.L)));
    err_cheby_lp2(ind_order) = norm(H-H_cheby,'fro')/norm(H,'fro');
end


%------------ Ideal low-pass 3
cutoff = 23;
H = U*diag(diag(Dtrue)<=Dtrue(cutoff,cutoff))*U';


err_givens_lp3 = zeros(1,numel(U_tab));
for ind_fourier = 1:numel(U_tab)
    Uhat = U_tab{ind_fourier};
    Dhat = D_tab{ind_fourier};
    H_givens = Uhat*diag(diag(Dhat)<=Dhat(cutoff,cutoff))*Uhat';
    err_givens_lp3(ind_fourier) = norm(H-H_givens,'fro')/norm(H,'fro');
end


err_cheby_lp3 = zeros(1,3);
RCG_cheby = zeros(1,3);
g=@(x) x<=Dtrue(cutoff,cutoff);
for ind_order = 1:numel(poly_order_tab)
    coeffs = gsp_cheby_coeff(G, g, poly_order_tab(ind_order));
    H_cheby = gsp_cheby_op(G, coeffs, eye(DIM));
    RCG_cheby(ind_order) = DIM^2./(poly_order_tab(ind_order)*(2*DIM+nnz(G.L)));
    err_cheby_lp3(ind_order) = norm(H-H_cheby,'fro')/norm(H,'fro');
end

%------------ Exponential
lambda = 1/20;
H = U*diag(exp(-lambda*diag(Dtrue)))*U';


err_givens_exp = zeros(1,numel(U_tab));
for ind_fourier = 1:numel(U_tab)
    Uhat = U_tab{ind_fourier};
    Dhat = D_tab{ind_fourier};
    H_givens = Uhat*diag(exp(-lambda*diag(Dhat)))*Uhat';
    err_givens_exp(ind_fourier) = norm(H-H_givens,'fro')/norm(H,'fro');
end


err_cheby_exp = zeros(1,3);
RCG_cheby = zeros(1,3);
g=@(x) exp(-lambda*x);
for ind_order = 1:numel(poly_order_tab)
    coeffs = gsp_cheby_coeff(G, g, poly_order_tab(ind_order));
    H_cheby = gsp_cheby_op(G, coeffs, eye(DIM));
    RCG_cheby(ind_order) = DIM^2./(poly_order_tab(ind_order)*(2*DIM+nnz(G.L)));
    err_cheby_exp(ind_order) = norm(H-H_cheby,'fro')/norm(H,'fro');
end

%% Building table VI
TableVI = [err_cheby_lp(1) err_givens_lp(1) err_cheby_lp(2) err_givens_lp(2) err_cheby_lp(3) err_givens_lp(3)  ;
    err_cheby_lp3(1) err_givens_lp3(1) err_cheby_lp3(2) err_givens_lp3(2) err_cheby_lp3(3) err_givens_lp3(3)  ;
    err_cheby_lp2(1) err_givens_lp2(1) err_cheby_lp2(2) err_givens_lp2(2) err_cheby_lp2(3) err_givens_lp2(3)  ;
    err_cheby_exp(1) err_givens_exp(1) err_cheby_exp(2) err_givens_exp(2) err_cheby_exp(3) err_givens_exp(3) ]
