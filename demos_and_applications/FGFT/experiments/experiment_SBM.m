%% Description experiment_SBM
%  Evaluation of the FGFTs performance on Stochastic Block Model graphs.
%
%  This script allows to reproduce the Table III of section IV.B of [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%

%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.lemagoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L., Gribonval R. and Tremblay N., "Approximate fast 
%   graph Fourier transforms via multi-layer sparse approximations", 
%   submitted to IEEE Transactions on Signal and Information Processing
%   over Networks.
%	<https://hal.inria.fr/hal-01416110>
%%


Ntrials=10; % number of independent trials
G.N = 1e3; % number of nodes 
DIM = G.N;
G.k = 20;  % number of communities 
%---- if homogeneous com size
com_size=round(ones(1,G.k).*(G.N/G.k));
deg_tab = [4,8,16,32];
%---- difficulty of community detection 
epsi_tab = [1/100,1/50,1/25,1/15,1/10,1/5,1/2];

Restab = zeros(numel(deg_tab),numel(epsi_tab),5,Ntrials); % Result table

for i=1:numel(deg_tab)
    disp(['Average degree: ' num2str(deg_tab(i))])
	G.c = deg_tab(i); % average degree

	for j=1:numel(epsi_tab)
        disp(['Relative epsilon: ' num2str(epsi_tab(j))])
		epsi_c=(G.c-sqrt(G.c))/(G.c+sqrt(G.c)*(G.k-1)); %maximum difficulty 
		epsi=epsi_c*epsi_tab(j); % the closer to espi_c, the less "strong" are the communities, ie, the more difficult is the clust. task (phase transition at epsilon_c)

		disp(['DIM = ' num2str(DIM)])
		RCG=10;
		J = round(DIM^2/(4*RCG));
	
		for k=1:Ntrials
			%---- create SBM graph
			[G.W,G.truth] = create_SBM(G.N,G.k,G.c,epsi,com_size);
			G.L = diag(sum(G.W,2))-G.W;
            d = eig(G.L);
			tic
		
			RCG = DIM^2/(4*J);
			norm_l0 = nnz(G.L);
			Restab(i,j,1,k)=norm_l0;
			offdiag = norm(G.L-diag(diag(G.L)),'fro');
			Restab(i,j,2,k)=offdiag;
			err_d = norm(G.L-diag(diag(G.L)),'fro')/norm(G.L,'fro');
			Restab(i,j,3,k)=err_d;
        
			%%% Greedy diagonalization (algo of fig.3)
			[facts,D_hat,err,L,coord_choices] = diagonalization_givens_parall(G.L,J,DIM/2);
			Restab(i,j,4,k)=sqrt(err(end));
            Restab(i,j,5,k)=norm(d-diag(D_hat))/norm(d);
		end
	end
end

%%% Save the results
heure = clock ;
save(['SBM' date '-' num2str(heure(4)) '-' num2str(heure(5))  ],'Restab');

%% Loading precomputed results (if you do not want to recompute everything)
%load('SBM')

%% Building table III
Tab_err_d = mean(Restab(:,[1 3 5 7],4,:),4);
Tab_err_e = mean(Restab(:,[1 3 5 7],5,:),4);
