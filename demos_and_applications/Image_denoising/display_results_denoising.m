%% Description display_results
%	Run the script "display_results_denoising.m" that displays the 
%	figures (and some tables not present in the paper) corresponding to 
%	the above experiments (cf. Fig. 12 from [1]).
%	If you decided *not* to re-run the complete experiment with 
%	Denoising_faust_ksvd.m or/and Denoising_dct.m, then the figure is 
%	obtained using the denoising results from the file
%	"./precomputed_results_denoising/results_denoising.mat"	or/and
%	"./precomputed_results_denoising/results_denoising_DCT.mat" 
%
%	Otherwise it is obtained from the file 
%	"./output/results_denoising_user.mat" or/and
%	"./output/results_denoising_DCT_user.mat".
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%

if (not(exist('precomputed_data')))
    precomputed_data='FALSE';
end

runPath=which(mfilename);
pathname = fileparts(runPath);
if ( exist(strcat(pathname,'/output/results_denoising_DCT_user.mat'),'file') && not (strcmp(precomputed_data,'TRUE' )))
    load (strcat(strcat(pathname,'/output/'),'results_denoising_DCT_user')) ;
    disp(strcat('Using your results from directory :',strcat(pathname,'/output/results_denoising_DCT_user.mat')));
elseif exist(strcat(pathname,'/precomputed_results_denoising/results_denoising_DCT.mat'),'file')
    %load 'results_denoising_DCT'
    load (strcat(strcat(pathname,'/precomputed_results_denoising/'),'results_denoising_DCT')) ;
    disp(strcat('Using existing results (from reference [1]) from directory :',strcat(pathname,'/precomputed_results_denoising/results_denoising_DCT.mat')));
else
    disp(['ERROR : A problem occurred when loading the results of results_denoising_DCT.']);
    break;
end

resultTab_DCT = resultTab;

if ( exist(strcat(pathname,'/output/results_denoising_user.mat'),'file') && not (strcmp(precomputed_data,'TRUE' )))
    load (strcat(strcat(pathname,'/output/'),'results_denoising_user')) ;
    disp(strcat('Using your results from directory :',strcat(pathname,'/output/results_denoising_user.mat')));
elseif exist(strcat(pathname,'/precomputed_results_denoising/results_denoising.mat'),'file')
    %load 'results_denoising'
    load (strcat(strcat(pathname,'/precomputed_results_denoising/'),'results_denoising')) ;
    disp(strcat('Using existing results (from reference [1]) from directory :',strcat(pathname,'/precomputed_results_denoising/results_denoising.mat')));
else
    disp(['ERROR : A problem occurred when loading the results of results_denoising.']);
    break;
end
% if exist('results_denoising_user','file')
%     load 'results_denoising_user'
% else
%     load 'results_denoising'
% end


resultTab(3,:,:,:,:,:,:,:) = resultTab_DCT(3,:,:,:,:,:,:,:);

RCs = resultTab(:,1,1,:,:,:,:,:);
RCs = permute(RCs,[4,1,2,3,5,6,7,8]);
RCs = RCs(:,:,:,:,:,:,:,3);
RCs = squeeze(RCs);

res_Cameraman = resultTab(:,2,1,:,:,:,:,:);
res_Cameraman = permute(res_Cameraman,[4,1,2,3,5,6,7,8]);
res_Cameraman = res_Cameraman(:,:,:,:,:,:,:,1);
res_Cameraman = squeeze(res_Cameraman);

table_Cameraman =  Tab_denoise(res_Cameraman);

Fig_denoise(res_Cameraman,RCs,'Cameraman')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_House = resultTab(:,2,2,:,:,:,:,:);
res_House = permute(res_House,[4,1,2,3,5,6,7,8]);
res_House = res_House(:,:,:,:,:,:,:,1);
res_House = squeeze(res_House);

table_House =  Tab_denoise(res_House);

Fig_denoise(res_House,RCs,'House')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_Jetplane = resultTab(:,2,3,:,:,:,:,:);
res_Jetplane = permute(res_Jetplane,[4,1,2,3,5,6,7,8]);
res_Jetplane= res_Jetplane(:,:,:,:,:,:,:,1);
res_Jetplane = squeeze(res_Jetplane);

table_Jetplane =  Tab_denoise(res_Jetplane);

Fig_denoise(res_Jetplane,RCs,'Jetplane')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_Lake = resultTab(:,2,4,:,:,:,:,:);
res_Lake = permute(res_Lake,[4,1,2,3,5,6,7,8]);
res_Lake= res_Lake(:,:,:,:,:,:,:,1);
res_Lake = squeeze(res_Lake);

table_Lake =  Tab_denoise(res_Lake);

Fig_denoise(res_Lake,RCs,'Lake')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_Lena = resultTab(:,2,5,:,:,:,:,:);
res_Lena = permute(res_Lena,[4,1,2,3,5,6,7,8]);
res_Lena= res_Lena(:,:,:,:,:,:,:,1);
res_Lena = squeeze(res_Lena);

table_Lena =  Tab_denoise(res_Lena);

Fig_denoise(res_Lena,RCs,'Lena')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_Livingroom = resultTab(:,2,6,:,:,:,:,:);
res_Livingroom = permute(res_Livingroom,[4,1,2,3,5,6,7,8]);
res_Livingroom= res_Livingroom(:,:,:,:,:,:,:,1);
res_Livingroom = squeeze(res_Livingroom);

table_Livingroom =  Tab_denoise(res_Livingroom);

Fig_denoise(res_Livingroom,RCs,'Livingroom')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_Mandrill = resultTab(:,2,7,:,:,:,:,:);
res_Mandrill = permute(res_Mandrill,[4,1,2,3,5,6,7,8]);
res_Mandrill= res_Mandrill(:,:,:,:,:,:,:,1);
res_Mandrill = squeeze(res_Mandrill);


table_Mandrill =  Tab_denoise(res_Mandrill);

Fig_denoise(res_Mandrill,RCs,'Mandrill')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_Peppers = resultTab(:,2,8,:,:,:,:,:);
res_Peppers = permute(res_Peppers,[4,1,2,3,5,6,7,8]);
res_Peppers= res_Peppers(:,:,:,:,:,:,:,1);
res_Peppers = squeeze(res_Peppers);

table_Peppers =  Tab_denoise(res_Peppers);

Fig_denoise(res_Peppers,RCs,'Peppers')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_Pirate = resultTab(:,2,9,:,:,:,:,:);
res_Pirate = permute(res_Pirate,[4,1,2,3,5,6,7,8]);
res_Pirate= res_Pirate(:,:,:,:,:,:,:,1);
res_Pirate = squeeze(res_Pirate);

table_Pirate =  Tab_denoise(res_Pirate);

Fig_denoise(res_Pirate,RCs,'Pirate')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_Walkbridge = resultTab(:,2,10,:,:,:,:,:);
res_Walkbridge = permute(res_Walkbridge,[4,1,2,3,5,6,7,8]);
res_Walkbridge= res_Walkbridge(:,:,:,:,:,:,:,1);
res_Walkbridge = squeeze(res_Walkbridge);

table_Walkbridge =  Tab_denoise(res_Walkbridge);

Fig_denoise(res_Walkbridge,RCs,'Walkbridge')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_WomanBlonde = resultTab(:,2,11,:,:,:,:,:);
res_WomanBlonde = permute(res_WomanBlonde,[4,1,2,3,5,6,7,8]);
res_WomanBlonde= res_WomanBlonde(:,:,:,:,:,:,:,1);
res_WomanBlonde = squeeze(res_WomanBlonde);

table_WomanBlonde =  Tab_denoise(res_WomanBlonde);

Fig_denoise(res_WomanBlonde,RCs,'WomanBlonde')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res_WomanDark = resultTab(:,2,12,:,:,:,:,:);
res_WomanDark = permute(res_WomanDark,[4,1,2,3,5,6,7,8]);
res_WomanDark= res_WomanDark(:,:,:,:,:,:,:,1);
res_WomanDark = squeeze(res_WomanDark);

table_WomanDark =  Tab_denoise(res_WomanDark);

Fig_denoise(res_WomanDark,RCs,'WomanDarkHair')
