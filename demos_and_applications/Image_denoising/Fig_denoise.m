%% Description function Fig_denoise(res_X,RCs,title_string)
%
%  This function builds the denoising figures used in [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%

function Fig_denoise(res_X,RCs,title_string)

RCs_128 = RCs(1,4,1,:,:);
RCs_128 = squeeze(RCs_128);
RCs_256 = RCs(1,4,2,:,:);
RCs_256 = squeeze(RCs_256);
RCs_512 = RCs(1,4,3,:,:);
RCs_512 = squeeze(RCs_512);
res_X_128 = res_X(:,:,1,:,:);
res_X_256 = res_X(:,:,2,:,:);
res_X_512 = res_X(:,:,3,:,:);
sigma = [10,15,20,30,50];
BM3D_lena = [35.93, 34.27, 33.05, 31.26, 29.05];
BM3D_pirate = [33.98, 31.93, 30.59, 28.86, 26.81];
BM3D_livingroom = [34.04, 32.11, 30.76, 28.87, 26.46];

figure('color',[1,1,1])
text(0,0,'Test title')
subplot_tight(1,3,1,[0.1,0.05])
for k=1:5 % noise level
    col = [(k-1)/4,1-(k-1)/4,0];
    p(k) = plot([0 1],[res_X_128(k,1,1,1,1), res_X_128(k,1,1,1,1)],'--','color',col,'displayname',['DDL, \sigma=' num2str(sigma(k))]);
    %plot([0 12000],[0,0],'--','color','k')
    hold on
    plot([0 12000],[0,0],'-','color','k')
    %plot([0 1],[res_X_128(k,2,1,1,1), res_X_128(k,2,1,1,1)],':','color',col)
    %plot([0 1],[res_X_128(k,3,1,1,1), res_X_128(k,3,1,1,1)],':','color',col)
    test = res_X_128(k,4,1,:,:) - res_X_128(k,1,1,1,1);
    test = squeeze(test);
    [B,I] = sort(RCs_128(:));
    q(k) = plot(B*128*64,test(I),'+:','color',col,'displayname',['FA\muST, \sigma=' num2str(sigma(k))]);
    
    if strcmp(title_string,'Lena')
        diff_BM3D = BM3D_lena(k) - res_X_128(k,1,1,1,1);
        r(k) = plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    if strcmp(title_string,'Pirate')
        diff_BM3D = BM3D_pirate(k) - res_X_128(k,1,1,1,1);
        r(k) = plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    if strcmp(title_string,'Livingroom')
        diff_BM3D = BM3D_livingroom(k) - res_X_128(k,1,1,1,1);
        r(k) = plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT %
    diff_DCT = res_X_128(k,3,1,1,1) - res_X_128(k,1,1,1,1);
    s(k) =  plot([0 12000],[diff_DCT,diff_DCT],'--','color',col,'displayname',['DCT, \sigma=' num2str(sigma(k))]);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    axis([0,12000,-6.5,10.5])
    set(gca,'fontsize',12)
    if exist('r'); leg=legend([r]); else leg=legend(q); end %leg=legend([q,r]);
    set(leg,'FontSize',14);
    xlabel('Number of parameters','Fontsize',14)
    ylabel('\DeltaPSNR wrt DDL (dB)','Fontsize',14)
end
title('128 atoms','FontWeight','normal','Fontsize',14)

subplot_tight(1,3,2,[0.1,0.05])
for k=1:5
    col = [(k-1)/4,1-(k-1)/4,0];
    plot([0 1],[res_X_256(k,1,1,1,1), res_X_256(k,1,1,1,1)],'--','color',col)
    plot([0 12000],[0,0],'-','color','k')
    hold on
    %plot([0 1],[res_X_256(k,2,1,1,1), res_X_256(k,2,1,1,1)],':','color',col)
    %plot([0 1],[res_X_256(k,3,1,1,1), res_X_256(k,3,1,1,1)],':','color',col)
    test = res_X_256(k,4,1,:,:)- res_X_256(k,1,1,1,1);
    test = squeeze(test);
    [B,I] = sort(RCs_256(:));
    plot(B*256*64,test(I),'b+:','color',col)
    if strcmp(title_string,'Lena')
        diff_BM3D = BM3D_lena(k) - res_X_256(k,1,1,1,1);
         plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    if strcmp(title_string,'Pirate')
        diff_BM3D = BM3D_pirate(k) - res_X_256(k,1,1,1,1);
         plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    if strcmp(title_string,'Livingroom')
        diff_BM3D = BM3D_livingroom(k) - res_X_256(k,1,1,1,1);
         plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT %
    diff_DCT = res_X_256(k,3,1,1,1) - res_X_256(k,1,1,1,1);
    s(k) =  plot([0 12000],[diff_DCT,diff_DCT],'--','color',col,'displayname',['DCT, \sigma=' num2str(sigma(k))]);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    leg2=legend([s]);
    axis([0,12000,-6.5,10.5])
    set(gca,'fontsize',12)
    xlabel('Number of parameters','Fontsize',14)
    %ylabel('PSNR (dB)')
end
title('256 atoms','FontWeight','normal','Fontsize',14)

subplot_tight(1,3,3,[0.1,0.05])
for k=1:5
    col = [(k-1)/4,1-(k-1)/4,0];
    plot([0 1],[res_X_512(k,1,1,1,1), res_X_512(k,1,1,1,1)],'--','color',col)
    plot([0 12000],[0,0],'-','color','k')
    hold on
    %plot([0 1],[res_X_512(k,2,1,1,1), res_X_512(k,2,1,1,1)],':','color',col)
    %plot([0 1],[res_X_512(k,3,1,1,1), res_X_512(k,3,1,1,1)],':','color',col)
    test = res_X_512(k,4,1,:,:)- res_X_512(k,1,1,1,1);
    test = squeeze(test);
    [B,I] = sort(RCs_512(:));
    plot(B*512*64,test(I),'b+:','color',col)
    if strcmp(title_string,'Lena')
        diff_BM3D = BM3D_lena(k) - res_X_512(k,1,1,1,1);
         plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    if strcmp(title_string,'Pirate')
        diff_BM3D = BM3D_pirate(k) - res_X_512(k,1,1,1,1);
         plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    if strcmp(title_string,'Livingroom')
        diff_BM3D = BM3D_livingroom(k) - res_X_512(k,1,1,1,1);
         plot([0 12000],[diff_BM3D,diff_BM3D],'-.','color',col,'displayname',['BM3D, \sigma=' num2str(sigma(k))]);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT DCT %
    diff_DCT = res_X_512(k,3,1,1,1) - res_X_512(k,1,1,1,1);
    s(k) =  plot([0 12000],[diff_DCT,diff_DCT],'--','color',col,'displayname',['DCT, \sigma=' num2str(sigma(k))]);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    axis([0,12000,-6.5,10.5])
    set(gca,'fontsize',12)
    xlabel('Number of parameters','Fontsize',14)
    %ylabel('PSNR (dB)')
end
title('512 atoms','FontWeight','normal','Fontsize',14)

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 ...
    1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

text(0.5, 1,title_string,'HorizontalAlignment'...
    ,'center','VerticalAlignment', 'top','Fontsize',16)

%savefig(title_string)
end