%% Description function latex_table = Tab_denoise(res_X)
% 
% This function builds the denoising tables used in [1].
%
% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%


function latex_table = Tab_denoise(res_X)

res_X2D = zeros(size(res_X,1),36);
for i=1:3*size(res_X,3)
    res_X2D(:,i) = res_X(:,floor((i-1)/3)+1,mod(i-1,3)+1,1,1);
end
for j=1:27
    res_X2D(:,j+9) = res_X(:,4,mod(j-1,3)+1,mod(floor((j-1)/3),3)+1,floor((j-1)/9)+1);
end

input.data = res_X2D;
input.dataFormat = {'%.2f'};
input.tableCaption = 'Caption';
latex_table = latexTable(input);