function SMALL_ImgDeNoiseResult(SMALL)   
%%  Represents the results of Dictionary Learning for Image denoising
%
%   Function gets as input SMALL structure and plots  Image Denoise
%   results: Original Image, Noisy Image and for learned dictionaries and 
%   denoised images 
%

%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2010 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%


figure('Name', sprintf('Image %s (training set size- %d, sigma - %d)',SMALL.Problem.name, SMALL.Problem.n, SMALL.Problem.sigma));

m=size(SMALL.solver,2)+1;
maxval=SMALL.Problem.maxval;
im=SMALL.Problem.Original;
imnoise=SMALL.Problem.Noisy;

subplot(2, m, 1); imagesc(im/maxval);colormap(gray);axis off; axis image;        % Set aspect ratio to obtain square pixels
title('Original image');

subplot(2,m,m+1); imagesc(imnoise/maxval);axis off; axis image; 
title(sprintf('Noisy image, PSNR = %.2fdB', SMALL.Problem.noisy_psnr ));

for i=2:m
    
    subplot(2, m, i); imagesc(SMALL.solver(i-1).reconstructed.Image/maxval);axis off; axis image; 
    title(sprintf('%s Denoised image, PSNR: %.2f dB in %.2f s',...
        SMALL.DL(i-1).name, SMALL.solver(i-1).reconstructed.psnr, SMALL.solver(i-1).time ),'Interpreter','none');
    if strcmpi(SMALL.DL(i-1).name,'ksvds')
        D = kron(SMALL.Problem.basedict{2},SMALL.Problem.basedict{1})*SMALL.DL(i-1).D;
    else
        D = SMALL.DL(i-1).D;
    end
    dictimg = SMALL_showdict(D,SMALL.Problem.blocksize,...
        round(sqrt(size(D,2))),round(sqrt(size(D,2))),'lines','highcontrast');
    
    subplot(2,m,m+i);imagesc(dictimg);axis off; axis image; 
    title(sprintf('%s dictionary in %.2f s',...
        SMALL.DL(i-1).name, SMALL.DL(i-1).time),'Interpreter','none');
    
end