function figAMT=SMALL_AMT_plot(SMALL, AMT_res)
%%   Function gets as input SMALL structure and plots AMT results
%

%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   



figAMT=figure('Name','Automatic Music Transcription');

m=size(AMT_res,2);

for i =1:m
    subplot(m,1, i);plot(AMT_res(i).tp_notes(:,2), AMT_res(i).tp_notes(:,1),'ko', ...
        AMT_res(i).tp_notes(:,4), AMT_res(i).tp_notes(:,3),'gx', ...
        AMT_res(i).oe_notes(:,2), AMT_res(i).oe_notes(:,1),'bo', ...
        AMT_res(i).oe_notes(:,4), AMT_res(i).oe_notes(:,3),'bx', ...
        AMT_res(i).fn_notes_wo_oe(:,2), AMT_res(i).fn_notes_wo_oe(:,1),'ro', ...
        AMT_res(i).fp_notes_wo_oe(:,2), AMT_res(i).fp_notes_wo_oe(:,1),'rx')
    title(sprintf('%s dictionary in %.2f s - TP=%d FN=%d (Octave Errors = %d) FP=%d', SMALL.DL(i).name, SMALL.DL(i).time, AMT_res(i).TP, AMT_res(i).FN, size(AMT_res(i).oe_notes,1), AMT_res(i).FP));
    xlabel('Time')
    ylabel('Note Number')
    
end
