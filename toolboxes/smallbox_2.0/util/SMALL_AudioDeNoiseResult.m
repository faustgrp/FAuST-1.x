function SMALL_AudioDeNoiseResult(SMALL)
%% Plots the results of Audio denoising experiment - underconstruction

%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   

fMain=figure('Name', sprintf('File %s (training set size- %d, sigma - %d)',SMALL.Problem.name, SMALL.Problem.n, SMALL.Problem.sigma));
m=size(SMALL.solver,2);
maxval=max(SMALL.Problem.Original);
au=SMALL.Problem.Original;
aunoise=SMALL.Problem.Noisy;

subplot(2, m, 1); plot(au/maxval);
title('Original audio');

subplot(2,m,2); plot(aunoise/maxval);
title(sprintf('Noisy audio, PSNR = %.2fdB', 20*log10(maxval * sqrt(numel(au)) / norm(au(:)-aunoise(:))) ));

for i=1:m
    params=SMALL.solver(i).param;
    sWav=subplot(2, m, m+i, 'Parent', fMain); plot(SMALL.solver(i).reconstructed.audio/maxval, 'Parent', sWav);
    title(sprintf('%s Denoised audio, PSNR: %.2fdB', SMALL.DL(i).name, SMALL.solver(i).reconstructed.psnr),'Parent', sWav );
    if strcmpi(SMALL.DL(i).name,'ksvds')
        D = kron(SMALL.Problem.basedict{2},SMALL.Problem.basedict{1})*SMALL.DL(i).D;
    else
        D = SMALL.DL(i).D;
    end
    figure('Name', sprintf('%s dictionary in %.2f s', SMALL.DL(i).name, SMALL.DL(i).time));
    imshow(D*255);
%     n= size(D,2);
%     sqrtn=round(sqrt(size(D,2)));
%     for j=1:n
%     subplot(sqrtn,sqrtn,j); plot(D(:,j));
%     end
%     dictimg = showdict(D,[params.blocksize 1],round(sqrt(size(D,2))),round(sqrt(size(D,2))),'lines','highcontrast');
%     
%     subplot(2,m,m+i);imshow(imresize(dictimg,2,'nearest'));
%     title(sprintf('%s dictionary in %.2f s', SMALL.DL(i-1).name, SMALL.DL(i-1).time));
    
end