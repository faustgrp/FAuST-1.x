function SMALL_playAudio(SMALL)
%%  Plays reconstructed and original signals
%   Function gets as input SMALL structure and play the original and
%   the reconstructed signal

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%   



SMALL.solver.reconstructed  = SMALL.Problem.reconstruct(SMALL.solver.solution);
ch='';
while 1
    request = input('\nWhat do you want to hear? \n 1. Original signal \n 2. Mixed \n 3. Reconstructed signal \n 4. Quit player\n','s');
    request = sscanf(request,'%d');
    switch request
        case 1
            fprintf('Original signal has %d sources.\n Which one do you want to hear?', size(SMALL.Problem.signal,2))
            fprintf('Enter a number between 1 and %d', size(SMALL.Problem.signal,2))
            ch=input('','s');
            ch=sscanf(ch,'%d');
            if (ch>=1)&&(ch<=size(SMALL.Problem.signal,2))
                soundsc(SMALL.Problem.signal(:,ch),8000);
            end
        case 2
            fprintf('Number of channels in mixed signal is %d.\n Which one do you want to hear?', size(SMALL.Problem.b,2))
            fprintf('Enter a number between 1 and %d', size(SMALL.Problem.signal,2))
            ch=input('','s');
            ch=sscanf(ch,'%d');
            if (ch>=1)&&(ch<=size(SMALL.Problem.b,2))
                soundsc(SMALL.Problem.b(:,ch),8000);
            end
        case 3
            fprintf('Reconstructed signal has %d sources.\n Which one do you want to hear?', size(SMALL.solver.reconstructed,2))
            fprintf('Enter a number between 1 and %d', size(SMALL.Problem.signal,2))
            ch=input('','s');
            ch=sscanf(ch,'%d');
            if (ch>=1)&&(ch<=size(SMALL.solver.reconstructed,2))
                soundsc(SMALL.solver.reconstructed(:,ch),8000);
            end
        case 4
            return;
    end
end
end
