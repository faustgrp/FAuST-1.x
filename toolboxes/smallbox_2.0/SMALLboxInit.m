%%  SMALLboxInit
%
%   SMALLbox Initialization
%
%   Important: If running SMALLBox for the first time, 
%   please run SMALLboxSetup instead

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic, Matthew Davies.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%
%%

global SMALL_path;
SMALL_path = fileparts(mfilename('fullpath'));

addpath(genpath(SMALL_path));


