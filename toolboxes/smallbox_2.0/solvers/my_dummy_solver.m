function [A]=my_dummy_solver(Dict,X, m,  maxNumCoef, errorGoal, varargin) 
%%  Template function that can be used for solver implementation 
%   
%   Sparse coding of a group of signals based on a given 
%   dictionary and specified number of atoms to use. 
%
%   input arguments:    Dict - the dictionary
%                       X - the signals to represent
%                       m - number of atoms in Dictionary
%                       errorGoal - the maximal allowed representation
%                                   error for each signal.
%
%   optional:      if Dict is function handle then Transpose Dictionary
%                  handle needs to be specified.
%
%   output arguments: A - sparse coefficient matrix.

%%  Change copyright notice as appropriate:
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%
