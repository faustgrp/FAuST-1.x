function x= thresh(x, K)

[~, ind] = sort(abs(x), 'descend');
x(ind(K+1:end))= 0;