%% Maximum A Posteriori Dictionary Learning with the constraint on the column norms %%%%%
function [Phiout,unhatnz] = mapfn(Phi,x,unhat,mu,maxIT,eps,phim,res)
K = Phi;
B = zeros(size(Phi,1),size(Phi,2));
i = 1;
while (sum(sum((B-K).^2))>eps)&&(i<=maxIT)   
    B = K;
    E = x-K*unhat;   
    K = K+mu*(phim*E*unhat'-trace(unhat*E'*K)*K);
    i = i+1;
end
%%% depleted atoms cancellation %%%
[Y,I] = sort(sum(K.^2),'descend');
RR = phim;
Phiout = K(:,I(1:RR));
unhatnz = unhat(I(1:RR),:);