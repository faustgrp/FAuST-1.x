function modcn_exactRec_demo(it,k,sn)
tic
IT = it;
K = k;
SN = sn;
if SN<10,
    samnum = ['0',num2str(SN)];
else
    samnum = num2str(SN);
end
load(['Param',num2str(K),'kS',samnum,'.mat'])
lambda = 2*.2; % 2 * Smallest coefficients (Soft Thresholding)
%%%%%%%%%%%%%%
Phi = Phio;
M = size(Phi,1);
[Phi,unhat,ert] = mod_cn(x,Phi,lambda,IT);

save(['MODl1',num2str(M),'t',num2str(IT),'ikiun',num2str(K),'v1d',num2str(SN),'.mat'],'Phi','Phid','x','ud','unhat','ert')

toc
