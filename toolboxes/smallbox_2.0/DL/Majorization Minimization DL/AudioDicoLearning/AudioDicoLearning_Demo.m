M = '256';
N = '512';
L = '8192'; % N*16;
IT = '100'; % For a full convergence at least '10000' iterations;
Lambda = '.01'; % Lagrangian multiplier
admiss = 'un'; % admissible set for dictionaries 'bn' = bounded norm, 'un' = unit norm
tic
DLMM_Audio(M,N,L,IT,Lambda,admiss)
toc