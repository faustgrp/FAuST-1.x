Majorization Minimization Dictionary Learning (MMDL) Package (Beta version)

This package includes the codes needed to generate some of the plots in the paper, "Dictionary Learning for Sparse Approximations with the Majorization Method" by M. Yaghoobi, T. Blumensath and M. Davies, IEEE Transaction on Signal Processing, Vol. 57, No. 6, pp 2178-2191, 2009.

The package also includes the authors' implementations of some other well-known dictionary learning methods. These have been used for comparison reason. 

The package includes two parts:

1- Building blocks of the MMDL:

	a) mm1 : Iterative Softthresholding for sparse approximation
	b) rdlcn : MMDL with column norm constraint
	c) rdlfn : MMDL with Frobenius norm constraint 
	d) softthreshold : Softthresholding operator
	e) Demo : A demo to demonstrate how to use MMDL and recover a generic dictionary

2- MMDL in action:
	a) ExactDicoRecovery : Exact generic atom recovery experiment. It includes a comparison with other methods
	b) Dictionary Learning for Audio : Dictionary learning with MMDL method applied to some classic music recorded from BBC radio 3

Installation:

You just need to decompress the package into your local disk and add the path of main directory to the Matlab path.

Citation: 

This package is free to use. However, any publication based on using this package, in any kind, should cite the mentioned paper.

Copyright: 

The use of this package does not need any farther permission, if it is restricted to the academic and industrial researches. Any military affiliated use of this package is strictly prohibited.
