%%  Dictionary Learning for Image Denoising - KSVD vs Recursive Least Squares
%
%   This file contains an example of how SMALLbox can be used to test different
%   dictionary learning techniques in Image Denoising problem.
%   It calls generateImageDenoiseProblem that will let you to choose image,
%   add noise and use noisy image to generate training set for dictionary
%   learning.
%   Two dictionary learning techniques were compared:
%   -   KSVD - M. Elad, R. Rubinstein, and M. Zibulevsky, "Efficient
%              Implementation of the K-SVD Algorithm using Batch Orthogonal
%              Matching Pursuit", Technical Report - CS, Technion, April 2008.
%   -   RLS-DLA - Skretting, K.; Engan, K.; , "Recursive Least Squares
%       Dictionary Learning Algorithm," Signal Processing, IEEE Transactions on,
%       vol.58, no.4, pp.2121-2130, April 2010
%


%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%   
%%



%   If you want to load the image outside of generateImageDenoiseProblem
%   function uncomment following lines. This can be useful if you want to
%   denoise more then one image for example.
%   Here we are loading test_image.mat that contains structure with 5 images : lena,
%   barbara,boat, house and peppers.
clear;
TMPpath=pwd;
FS=filesep;
[pathstr1, name, ext] = fileparts(which('SMALLboxSetup.m'));
cd([pathstr1,FS,'data',FS,'images']);
load('test_image.mat');
cd(TMPpath);

%   Deffining the noise levels that we want to test

noise_level=[10 20 25 50 100];

%   Here we loop through different noise levels and images 

for noise_ind=4:4
for im_num=1:1

% Defining Image Denoising Problem as Dictionary Learning
% Problem. As an input we set the number of training patches.

SMALL.Problem = generateImageDenoiseProblem(test_image(im_num).i, 40000, '',256, noise_level(noise_ind));
SMALL.Problem.name=int2str(im_num);

Edata=sqrt(prod(SMALL.Problem.blocksize)) * SMALL.Problem.sigma * SMALL.Problem.gain;
maxatoms = floor(prod(SMALL.Problem.blocksize)/2);

%   results structure is to store all results

results(noise_ind,im_num).noisy_psnr=SMALL.Problem.noisy_psnr;

%%
%   Use KSVD Dictionary Learning Algorithm to Learn overcomplete dictionary

%   Initialising Dictionary structure
%   Setting Dictionary structure fields (toolbox, name, param, D and time)
%   to zero values

SMALL.DL(1)=SMALL_init_DL();

% Defining the parameters needed for dictionary learning

SMALL.DL(1).toolbox = 'KSVD';
SMALL.DL(1).name = 'ksvd';

%   Defining the parameters for KSVD
%   In this example we are learning 256 atoms in 20 iterations, so that
%   every patch in the training set can be represented with target error in
%   L2-norm (Edata)
%   Type help ksvd in MATLAB prompt for more options.


SMALL.DL(1).param=struct(...
    'Edata', Edata,...
    'initdict', SMALL.Problem.initdict,...
    'dictsize', SMALL.Problem.p,...
    'exact', 1, ...
    'iternum', 20,...
    'memusage', 'high');

%   Learn the dictionary

SMALL.DL(1) = SMALL_learn(SMALL.Problem, SMALL.DL(1));

%   Set SMALL.Problem.A dictionary
%   (backward compatiblity with SPARCO: solver structure communicate
%   only with Problem structure, ie no direct communication between DL and
%   solver structures)

SMALL.Problem.A = SMALL.DL(1).D;
SMALL.Problem.reconstruct = @(x) ImageDenoise_reconstruct(x, SMALL.Problem);

%%
%   Initialising solver structure
%   Setting solver structure fields (toolbox, name, param, solution,
%   reconstructed and time) to zero values

SMALL.solver(1)=SMALL_init_solver;

% Defining the parameters needed for image denoising

SMALL.solver(1).toolbox='ompbox';
SMALL.solver(1).name='omp2';
SMALL.solver(1).param=struct(...
    'epsilon',Edata,...
    'maxatoms', maxatoms); 

%   Denoising the image - find the sparse solution in the learned
%   dictionary for all patches in the image and the end it uses
%   reconstruction function to reconstruct the patches and put them into a
%   denoised image

SMALL.solver(1)=SMALL_solve(SMALL.Problem, SMALL.solver(1));

%   Show PSNR after reconstruction

SMALL.solver(1).reconstructed.psnr

%%
%   For comparison purposes we will denoise image with overcomplete DCT
%   here
%   Set SMALL.Problem.A dictionary to be oDCT (i.e. Problem.initdict -
%   since initial dictionaruy is already set to be oDCT when generating the
%   denoising problem


%   Initialising solver structure
%   Setting solver structure fields (toolbox, name, param, solution,
%   reconstructed and time) to zero values

SMALL.solver(2)=SMALL_init_solver('ALPS','AgebraicPursuit','',1);

% Defining the parameters needed for image denoising

SMALL.solver(2).param=struct(...
    'tolerance',1e-05,...
    'sparsity', 32,...
    'mode', 0,...
    'memory', 1,...
    'iternum', 50); 

%   Initialising Dictionary structure
%   Setting Dictionary structure fields (toolbox, name, param, D and time)
%   to zero values

SMALL.DL(2)=SMALL_init_DL('TwoStepDL', 'Mailhe', '', 1);


%   Defining the parameters for MOD
%   In this example we are learning 256 atoms in 20 iterations, so that
%   every patch in the training set can be represented with target error in
%   L2-norm (EData)
%   Type help ksvd in MATLAB prompt for more options.


SMALL.DL(2).param=struct(...
    'solver', SMALL.solver(2),...
    'initdict', SMALL.Problem.initdict,...
    'dictsize', SMALL.Problem.p,...
    'iternum', 40,...
    'show_dict', 1);

%   Learn the dictionary

SMALL.DL(2) = SMALL_learn(SMALL.Problem, SMALL.DL(2));

%   Set SMALL.Problem.A dictionary
%   (backward compatiblity with SPARCO: solver structure communicate
%   only with Problem structure, ie no direct communication between DL and
%   solver structures)

SMALL.Problem.A = SMALL.DL(2).D;
SMALL.Problem.reconstruct = @(x) ImageDenoise_reconstruct(x, SMALL.Problem);

%   Denoising the image - find the sparse solution in the learned
%   dictionary for all patches in the image and the end it uses
%   reconstruction function to reconstruct the patches and put them into a
%   denoised image

SMALL.solver(2)=SMALL_solve(SMALL.Problem, SMALL.solver(2));

%%
% In the b1 field all patches from the image are stored. For RLS-DLA we
% will first exclude all the patches that have l2 norm smaller then
% threshold and then take min(40000, number_of_remaining_patches) in
% ascending order as our training set (SMALL.Problem.b)

X=SMALL.Problem.b1;
X_norm=sqrt(sum(X.^2, 1));
[X_norm_sort, p]=sort(X_norm);
p1=p(X_norm_sort>Edata);
if size(p1,2)>40000
    p2 = randperm(size(p1,2));
    p2=sort(p2(1:40000));
    size(p2,2)
    SMALL.Problem.b=X(:,p1(p2));
else 
    size(p1,2)
    SMALL.Problem.b=X(:,p1);

end

%   Forgetting factor for RLS-DLA algorithm, in this case we are using
%   fixed value

lambda=0.9998

%   Use Recursive Least Squares
%   to Learn overcomplete dictionary 

%   Initialising Dictionary structure
%   Setting Dictionary structure fields (toolbox, name, param, D and time)
%   to zero values

SMALL.DL(3)=SMALL_init_DL();

%   Defining fields needed for dictionary learning

SMALL.DL(3).toolbox = 'SMALL';
SMALL.DL(3).name = 'SMALL_rlsdla';
SMALL.DL(3).param=struct(...
    'Edata', Edata,...
    'initdict', SMALL.Problem.initdict,...
    'dictsize', SMALL.Problem.p,...
    'forgettingMode', 'FIX',...
    'forgettingFactor', lambda,...
    'show_dict', 1000);


SMALL.DL(3) = SMALL_learn(SMALL.Problem, SMALL.DL(3));

%   Initialising solver structure
%   Setting solver structure fields (toolbox, name, param, solution,
%   reconstructed and time) to zero values

SMALL.Problem.A = SMALL.DL(3).D;
SMALL.Problem.reconstruct = @(x) ImageDenoise_reconstruct(x, SMALL.Problem);

SMALL.solver(3)=SMALL_init_solver;

% Defining the parameters needed for image denoising

SMALL.solver(3).toolbox='ompbox';
SMALL.solver(3).name='omp2';
SMALL.solver(3).param=struct(...
    'epsilon',Edata,...
    'maxatoms', maxatoms); 


SMALL.solver(3)=SMALL_solve(SMALL.Problem, SMALL.solver(3));

SMALL.solver(3).reconstructed.psnr


% show results %

SMALL_ImgDeNoiseResult(SMALL);

results(noise_ind,im_num).psnr.ksvd=SMALL.solver(1).reconstructed.psnr;
results(noise_ind,im_num).psnr.odct=SMALL.solver(2).reconstructed.psnr;
results(noise_ind,im_num).psnr.rlsdla=SMALL.solver(3).reconstructed.psnr;
results(noise_ind,im_num).vmrse.ksvd=SMALL.solver(1).reconstructed.vmrse;
results(noise_ind,im_num).vmrse.odct=SMALL.solver(2).reconstructed.vmrse;
results(noise_ind,im_num).vmrse.rlsdla=SMALL.solver(3).reconstructed.vmrse;
results(noise_ind,im_num).ssim.ksvd=SMALL.solver(1).reconstructed.ssim;
results(noise_ind,im_num).ssim.odct=SMALL.solver(2).reconstructed.ssim;
results(noise_ind,im_num).ssim.rlsdla=SMALL.solver(3).reconstructed.ssim;

results(noise_ind,im_num).time.ksvd=SMALL.solver(1).time+SMALL.DL(1).time;
results(noise_ind,im_num).time.rlsdla.time=SMALL.solver(3).time+SMALL.DL(3).time;
clear SMALL;
end
end
% save results.mat results
