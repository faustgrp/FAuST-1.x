%%  Example test of solvers from different toolboxes on Sparco problem 6
%
%   The main purpose of this example is to show how to use SMALL structure
%   to solve SPARCO compressed sensing problems (1-11) and compare results
%   from different solvers.
%   To generate SMALL.Problem part of structure you can use generateProblem
%   function from Sparco toolbox giving the problem number and any
%   additional parameters you might want to change. Alternatively, you can
%   might want to consult sparco documentation to write a problem by
%   yourself. There are four fields the must be specified in SMALL.Problem 
%   - A, b, sizeA and reconstruct.
%   
%   To generate SMALL.solver part of the structure you must specify three
%   fields:
%   
%       SMALL.solver.toolbox - string with toolbox name is needed because
%                              different toolboxes are calling solver 
%                              functions in different ways.
%       SMALL.solver.name - its string representing solver name (e.g.
%                           SolveOMP)
%       SMALL.solver.param - string that contains optional parameters for
%                            particular solver (all parameters you want to
%                            specify except A, b and size of solution)
%                            
%   Every call to SMALL_solve function will generate following output:
%
%       SMALL.solver.solution - contains solution vector x
%       SMALL.solver.reconstructed - vector containing signal reconstructed
%                                    from the solution
%       SMALL.solver.time - time that solver spent to find the solution
%           
%   SMALL_plot function plots the SMALL.solver.solution and reconstructed
%   against original signal.
%   
%   In this particular example we are testing SMALL_cgp, SMALL_chol, 
%   SolveOMP form SparseLab and greed_pcgp form Sparsify against "PROB006  
%   Daubechies basis, Gaussian ensemble measurement basis, piecewise cubic 
%   polynomial signal" from Sparco. 
%   
%


%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%   

fprintf('\n\nExample test of SMALL solvers against their counterparts on Sparco problems.\n\n');

%%
% Generate SPARCO problem 
clear  

SMALL.Problem = generateProblem(6, 'P', 6, 'm', 2*500,'n',2*1024, 'show');

SMALL.Problem.A = opToMatrix(SMALL.Problem.A, 1);

%%
i=1;
%%
% ALPS test
SMALL.solver(i) = SMALL_init_solver;
SMALL.solver(i).toolbox = 'ALPS';
SMALL.solver(i).name = 'AlgebraicPursuit';

% In the following string all parameters except matrix, measurement vector
% and size of solution need to be specified. If you are not sure which
% parameters are needed for particular solver type "help <Solver name>" in
% MATLAB command line

SMALL.solver(i).param=struct(...
    'sparsity', 125,... 
    'memory', 0,...
    'mode', 1,...
    'iternum', 50,... 
    'tolerance', 1e-14');

SMALL.solver(i)=SMALL_solve(SMALL.Problem,SMALL.solver(i));


i=i+1;
%%
% SMALL Conjugate Gradient test 
SMALL.solver(i)=SMALL_init_solver;
SMALL.solver(i).toolbox='SMALL';    
SMALL.solver(i).name='SMALL_pcgp';

% In the following string all parameters except matrix, measurement vector
% and size of solution need to be specified. If you are not sure which
% parameters are needed for particular solver type "help <Solver name>" in
% MATLAB command line

SMALL.solver(i).param='200, 1e-14';

SMALL.solver(i)=SMALL_solve(SMALL.Problem,SMALL.solver(i));


i=i+1;

%%
% SolveOMP from SparseLab test 

SMALL.solver(i)=SMALL_init_solver;
SMALL.solver(i).toolbox='SparseLab';  
SMALL.solver(i).name='SolveOMP';

% In the following string all parameters except matrix, measurement vector
% and size of solution need to be specified. If you are not sure which
% parameters are needed for particular solver type "help <Solver name>" in
% MATLAB command line

SMALL.solver(i).param='200, 0, 0, 0, 1e-14';

SMALL.solver(i)=SMALL_solve(SMALL.Problem, SMALL.solver(i));

i=i+1;
  
%%
% SMALL OMP with Cholesky update test 
SMALL.solver(i)=SMALL_init_solver;
SMALL.solver(i).toolbox='SMALL';    
SMALL.solver(i).name='SMALL_chol';

% In the following string all parameters except matrix, measurement vector
% and size of solution need to be specified. If you are not sure which
% parameters are needed for particular solver type "help <Solver name>" in
% MATLAB command line

SMALL.solver(i).param='200, 1e-14';

SMALL.solver(i)=SMALL_solve(SMALL.Problem, SMALL.solver(i));

i=i+1;

%%
% greed_pcgp from Sparsify test 

SMALL.solver(i)=SMALL_init_solver;
SMALL.solver(i).toolbox='Sparsify';  
SMALL.solver(i).name='greed_pcgp';

% In the following string all parameters except matrix, measurement vector
% and size of solution need to be specified. If you are not sure which
% parameters are needed for particular solver type "help <Solver name>" in
% MATLAB command line

SMALL.solver(i).param='''stopCrit'', ''M'', ''stopTol'', 200';

SMALL.solver(i)=SMALL_solve(SMALL.Problem, SMALL.solver(i));

%%

SMALL_plot(SMALL);
  

  
 

