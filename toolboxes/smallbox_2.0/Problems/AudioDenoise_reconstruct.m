function reconstructed=AudioDenoise_reconstruct(y, Problem)
%%  Audio denoising Problem reconstruction function
%   
%   This reconstruction function is using sparse representation y 
%   in dictionary Problem.A to reconstruct denoised audio.
%   The output structre has following fields:
%       audio   - denoised audio signal
%       psnr    - psnr of the reconstructed audio signal
%
%   [1] I. Damnjanovic, M. E. P. Davies, and M. P. Plumbley "SMALLbox - an 
%   evaluation framework for sparse representations and dictionary 
%   learning algorithms," V. Vigneron et al. (Eds.): LVA/ICA 2010, 
%   Springer-Verlag, Berlin, Germany, LNCS 6365, pp. 418-425

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2011 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%

windowSize = Problem.windowSize;
overlap = Problem.overlap;
ws = Problem.ws(windowSize);
wa = Problem.wa(windowSize);

A = Problem.A;

orig   = Problem.Original;
noisy  = Problem.Noisy;


% reconstruct audio frames

xFrames = diag(ws)*(A*y);
wNormFrames = (ws.*wa)'*ones(1,size(xFrames,2));

%   overlap and add

rec   = col2imstep(xFrames, size(noisy), [windowSize 1], [windowSize*overlap 1]);
wNorm = col2imstep(wNormFrames, size(noisy), [windowSize 1], [windowSize*overlap 1]); 
wNorm(find(wNorm==0)) = 1; 
recN  = rec./wNorm;

%% output structure image+psnr %%
reconstructed.audio  = recN;
reconstructed.psnr = 20*log10(sqrt(numel(orig)) / norm(orig - reconstructed.audio));

end