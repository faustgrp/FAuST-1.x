function reconstructed=Pierre_reconstruct(y, Problem)
%%  Pierre Villars Example - reconstruction function
%
%   This example is based on the experiment suggested by Professor Pierre
%   Vandergheynst on the SMALL meeting in Villars.
%   The function is using sparse representation y in dictionary Problem.A 
%   to reconstruct the patches of the target image.

%
%   Centre for Digital Music, Queen Mary, University of London.
%   This file copyright 2009 Ivan Damnjanovic.
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License as
%   published by the Free Software Foundation; either version 2 of the
%   License, or (at your option) any later version.  See the file
%   COPYING included with this distribution for more information.
%%   




imout=Problem.A*y;

%   combine the patches into reconstructed image

im=col2im(imout,Problem.blocksize,size(Problem.imageTrg),'disctint');

%   bound the pixel values to [0,255] range 
im(im<0)=0;
im(im>255)=255;

%% output structure image+psnr %%
reconstructed.image=im;
reconstructed.psnr = 20*log10(Problem.maxval * sqrt(numel(Problem.imageTrg(:))) / norm(Problem.imageTrg(:)-im(:)));
end