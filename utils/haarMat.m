%% Description haarMat
%  Computation of tha Haar matrix and its "native" factorization
%  [D, Fact] = haarMat(n) computes the Haar matrix H of size 
%  2^n*2^n and its factorization Fact.

% For more information on the FAuST Project, please visit the website of 
% the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%

function [D, Fact] = haarMat(n)
Fact = zeros(2^n,2^n,n);
D = eye(2^n);

size_eye = 0;
for i=1:n
    X = eye(2^(n-i+1));
diffX = [downsample(diff(X)~=0,2);downsample(diff(X),2)]/sqrt(2);
if i==n
    diffX = [1 1;1 -1]/sqrt(2);
end

Fact(:,:,i) = blkdiag(diffX,eye(size_eye));
size_eye = size_eye + 2^(n-i);

end
Fact = permute(Fact,[2,1,3]);

for j=1:n
    D = D*Fact(:,:,j);
end
end