function [D, facts]= rand_dic(n_facts,dim,sparsity)
%% Description rand_dic
%  Generation of a random factorizable dictionary.
%  [D, facts]= rand_dic(n_facts,dim,sparsity) generates a random dictionary
%  D which is the product of n_facts factors of size dim*dim gathered in 
%  facts. each factor has exactly sparsity non-zero entries, is full-rank, 
%  its support is drawn uniformly at random and its non-zero entries are 
%  iid Gaussian.
%  
%  For more information on the FAuST Project, please visit the website of 
%  the project :  <http://faust.gforge.inria.fr>
%
%% License:
% Copyright (2016):	Luc Le Magoarou, Remi Gribonval
%			INRIA Rennes, FRANCE
%			http://www.inria.fr/
%
% The FAuST Toolbox is distributed under the terms of the GNU Affero 
% General Public License.
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published 
% by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful, but 
% WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
% See the GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%% Contacts:	
%	Luc Le Magoarou: luc.le-magoarou@inria.fr
%	Remi Gribonval : remi.gribonval@inria.fr
%
%% References:
% [1]	Le Magoarou L. and Gribonval R., "Flexible multi-layer sparse 
%	approximations of matrices and applications", Journal of Selected 
%	Topics in Signal Processing, 2016.
%	<https://hal.archives-ouvertes.fr/hal-01167948v1>
%%

D = eye(dim);
facts = zeros(dim,dim,n_facts);

for i = 1:n_facts
    rank_temp = 0;
    while rank_temp ~= dim
        temp = zeros(dim);
        indices = randperm(dim*dim);
        temp(indices(1:sparsity)) = randn(sparsity,1);
        rank_temp = rank(temp);
    end
    temp = normc(temp);
    facts(:,:,i) = temp;
    D = D*temp;
end

facts(:,:,n_facts) = facts(:,:,n_facts)*diag(sqrt(1./sum(D.^2,1)));
D = normc(D);
end