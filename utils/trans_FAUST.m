function D_T = trans_FAUST(D)

M = size(D,2);
D_T = cell(1,M);
if M==0
    D_T = D;
else        
    for i = 1:M
        D_T{i} = D{M-i+1}';
    end
end
end